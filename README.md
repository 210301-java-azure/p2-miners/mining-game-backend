# Miner Might Backend

With MinerMight, users are able to log in or register for an account where they will be provided with a miner(player account) and a mine. Players will be able to mine one of 3 resources: oil, iron, or copper. Players can then trade their resources to collect enough to craft items such as drills, trucks, and tarps. These items can improve efficiency of their mining or reduce the effects of weather, which link to the real world local weather of the player!

### Mine Types

There are 3 types of resources you can mine, `OIL`, `IRON`, and `COPPER`.

> When interacting with the backend, the mine types are case sensitive.

# [Exposed Endpoints](ENDPOINTS.md)

- [Login](#login)
- [User Account Management](#user-account-management)
- [Miner Information And Actions](#miner-information-and-actions)
- [Mine Access](#mine-access)
- [Crafted Items](#crafted-items)
- [Secured Endpoints](#secured-endpoints)

## Login

`POST /login` with a username and password passed by json.

Request body example:

```javascript
{
  "username": "exampleUser",
  "password": "examplePassword"
}
```

This will log a user in and return a JWT in a response header `miner-token`. This token is used to authorize users at other endpoints by attatching it as a request header with key `miner-token`.

---

## User Account Management

### Create An Account

`POST /users?mine-type=MineType` with a json object holding username, email, password, and zipcode.

> The [MineType](#mine-types) defines what resource the user will mine, and can be `OIL`, `IRON`, or `COPPER`. (case-sensitive)

Request body example:

```javascript
{
  "username": "newUser",
  "password": "password",
  "email":    "useremail@example.com",
  "zipCode":  "12345"
}
```

> The zip code is validated and the user will not be created if the zip code is invalid.

> The username must be unique.

If the user is successfully created, then a miner and mine will be created and assigned to the user, and all will be returned as json. You must then [login](#login) to use the service

---

### Update An Account [(Secured)](#user-secured)

`PUT /accounts/:id` with the fields you wish to update as json in the body. The id field should match the user id returned when.

```javascript
{
  "zipCode": "98765",
  "email":   "newemail@updated.com"
}
```

> The zip code is also validated on account update, so if you provide a zip code that is invalid, the update will fail.

Returns the [updated user as json](#user-object-response-body-example)

---

### Get User Information [(Secured)](#user-secured)

`GET /users/:id` The id field should match the user id of the [logged in](#login) user.

Returns the [requested user as a json object](#user-object-response-body-example)

---

### Delete An Account [(Secured)](#user-secured)

`DELETE /users/:id` The id field should match the user id of the [logged in](#login) user.

No response body, and simply uses status codes to indicate successful or failed deletion.

> Deletes the User information, as well as the Miner and Mine information associated with that user.

### Make A User Admin [(Admin Secured)](#admin-secured)

`GET /users/:id/set-admin?admin=true` Admin only endpoint to promote and demote other users to admin or non-admin roles.

Returns the [updated user as json](#user-object-response-body-example)

---

### Get All Users Information [(Admin Secured)](#admin-secured)

`GET /users` requests all users information. Only admins can view all this data, so a `miner-token` header is required

Returns a list of [user information](#user-object-response-body-example).

```javascript
[
  {
    "id": 1,
    "username": "User1",
    "...": "...",
  },
  {
    "id": 2,
    "username": "User2",
    "...": "...",
  },
  ...
]
```

---

### User Object Response Body Example:

```javascript
{
    "id": 21,
    "username": "new user",
    "email": "new@new.com",
    "password": "$2a$12$FJSh2SQlINasC564fe92suF15D/kfvnCgtyl32Eg3KuepfLy10Awe",
    "miner": {
        "id": 21,
        "mine": {
            "id": 21,
            "mineType": "IRON",
            "amountPerInterval": 5.0
        },
        "oil": 0.0,
        "iron": 6355.0,
        "copper": 0.0,
        "craftedItems": []
    },
    "zipCode": "12345",
    "admin": false
}
```

---

## Miner Information And Actions

### Get All Miners

`GET /miners`
Returns a list of all [miners](#miner-object-response-body-example)

```javascript
[
  {
    "id": 1,
    "mine": {
      ...,
    },
    ...,
  },
  {
    "id": 2,
    "mine": {
      ...,
    },
    ...
  },
  ...
]
```

---

### Get One Miner

`GET /miners/:id` Get a single miner by and id.

Returns a single [miner as json](#miner-object-response-body-example)

---

### Update Miner [(Admin Secured)](#admin-secured)

`PUT /miners/:id` Update a miner directly. User actions will update miners in other endpoints, so direct editing is only open to admins.

Returns the [updated miner as json](#miner-object-response-body-example)

---

### Trade Resources [(Secured)](#user-secured)

`GET /:id/trade?supply-type=MineType&supply-amount=123&return-type=MineType`
The [MineTypes](#mine-types) must be valid and must not be the same type.
The `supply-amount` must be a positive number.

> The query parameters `supply-type`, `return-type`, and `supply-amount` are required

An [updated miner object](#miner-object-response-body-example) is returned.

---

### Update Resources From Clicker

`GET /:id/add?resource-amount=123` allows the front-end clicker to add resources, and provides a chance to find a findable object.

Returns an [updated miner object](#miner-object-response-body-example)

---

### Get Current Efficiency or Weather Conditions [(Secured)](#user-secured)

`GET /miners/:id/efficiency` Get the current efficiency of the miner (how quickly they mine resources)

`GET /miners/:id/weather` Get the current weather conditions of the user. Returns a json of some weather information from the [OpenWeather api](https://openweathermap.org/)

Returned json example

```javascript
{
    "Temp": 15.99,
    "Effect Reduction": 0.15000000000000002,
    "Weather effect": 1.0,
    "Icon": "01d"
}
```

---

### Leaderboard

`GET /miners/leaderboard`
Returns a sorted array of json holding the username and total value of that user in json.

> The array is sorted by total value of all items and resources the miner owns. Each resource is scaled so no type of mine has an advantage. Value of duplicate items match the scaled cost of crafting those items.

Response Body Example:

```javascript
[
  {
    "username": "user3",
    "score": 123456789
  },
  {
    "username": "user13",
    "score": 123456000
  },
  ...
]
```

---

### Miner Object Response Body Example

```javascript
{
    "id": 2,
    "mine": {
        "id": 2,
        "mineType": "IRON",
        "amountPerInterval": 5.0
    },
    "oil": 20.0,
    "iron": 33230.0,
    "copper": 100.0,
    "craftedItems": [
        {
            "name": "Drill",
            "amountPerIntervalMultiplier": 0.5,
            "weatherEffectsModifier": 0.0,
            "oilNeeded": 100.0,
            "ironNeeded": 250.0,
            "copperNeeded": 500.0,
            "description": "Drills increase efficiency by 1.5",
            "cosmetic": false
        }
    ]
}
```

---

## Mine Access

### Get One or All

`GET /mines` to get a list of all mines.

`GET /mines/:id` to get a specific mine.

Response Body Example:

```javascript
{
  "id": 1,
  "mineType": "COPPER",
  "amountPerInterval": 10.0
}
```

---

### Update A Mine [(Admin Secured)](#admin-secured)

`PUT /mines/:id` with a request body json of what fields should be updated
Since users mines are updated and handled elsewhere, only admins can directly adjust a mine.

---

## Crafted Items

### Get All Available Items

`GET /items`
Returns a list of [crafted items](#get-single-item)

---

### Get Single Item

`GET /items/:name`
Returns a single crafted item, identified by the name of the item

Response Body Example:

```javascript
{
    "name": "drill",
    "amountPerIntervalMultiplier": 0.5,
    "weatherEffectsModifier": 0.0,
    "oilNeeded": 100.0,
    "ironNeeded": 250.0,
    "copperNeeded": 500.0,
    "description": "Drills increase efficiency by 1.5",
    "cosmetic": false
}
```

---

### Craft An Item

`GET /items/:name/craft?miner-id=123` The `miner-id` should match the id of the miner you own.
This crafts the item if you have enough resources to, and adds it to your miners list of crafted items.

---

### Add A Crafted Item To The Game [(Admin Secured)](#admin-secured)

`POST /items` with a json request body specifying what the item should look like.
This is a major aspect of the game, and only admins can create new items that users can craft.

# Secured Endpoints

### User Secured

Multiple endpoints are only accessible by the user who owns those resources. Only you (or an admin) can view your account information. And only you (or an admin) can perform certain gameply actions that affect your miner (like trading your resources)

These endpoints require a valid JWT obtained from [logging in](#login) placed into a response header named `miner-token`.

### Admin Secured

Some endpoints are only accessible by admin users. Similarly these endpoints require a valid JWT obtained from [logging in](#login) placed into a response header named `miner-token`.
