FROM java:8
COPY build/libs/mining-game-backend-0.0.1-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar mining-game-backend-0.0.1-SNAPSHOT.jar