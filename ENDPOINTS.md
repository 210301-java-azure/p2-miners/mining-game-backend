# Endpoints and requests

|     Endpoints      |    Request    |     Expected      |  Expected |
|:------------------:|:-------------:|:-----------------:|:---------:|
|/login              |      post     |     UserObject    |           |
|/users              |      get      |                   |           |
|/users              |      post     |     UserObject    |           |
|/users/:idnum       |      get      |                   |           |
|/users/:idnum       |      put      |     UserObject    |           |
|/users/:idnum       |     delete    |     UserObject    |           |
|/miners             |      get      |                   |           |
|/miners             |      post     |     MinerObject   | DO NOT USE|
|/miners/:idnum      |      get      |                   |           |
|/miners/:idnum      |      put      |     MinerObject   | ADMIN ONLY|
|/mine               |      get      |     MineObject    |           |
|/mine               |      post     |     MineObject    | DO NOT USE|
|/mine/:idnum        |      get      |                   |           |
|/mine/:idnum        |      put      |     MineObject    | ADMIN ONLY|
|/findables          |      get      |     FindableObj   |           |
|/findables          |      post     |     FindableObj   | ADMIN ONLY|
|/findables/:idnum   |      get      |                   |           |
|/findables/:idnum   |      put      |     FindableObj   | ADMIN ONLY|
|/items              |      get      |    CraftedItemObj |           |
|/items              |      post     |    CraftedItemObj | ADMIN ONLY|
|/items/:idnum       |      get      |                   |           |
|/items/:idnum       |      put      |    CraftedItemObj | ADMIN ONLY|
|/items/:idnum       |      put      |    CraftedItemObj | ADMIN ONLY|
|/items/:Name/craft  |      get      |      miner-id     |           |
|/miners/:idNum/trade|      get      |     supply-type   |           |
|                    |               |    supply-amount  |           |
|                    |               |     return-type   |           |
|/miners/:idNum/add  |      get      |   resource-amount |           |
