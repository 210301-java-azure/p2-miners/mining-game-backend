package dev.miners.models;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MinerComparatorTest {

	private static MinerComparator minerComparator;
	private static final CraftedItem drill = new CraftedItem("Drill", 0.5, 100, 250, 500, "desc");
	private static final CraftedItem tarps = new CraftedItem("Tarps", 0.5, 100, 250, 500, "desc");
	private static final CraftedItem truck = new CraftedItem("Truck", 0.5, 500, 1000, 2500, "desc");

	@BeforeAll
	static void setup(){
		HashMap<String, CraftedItem> map = new HashMap<>();
		map.put(drill.getName(), drill);
		map.put(tarps.getName(), tarps);
		map.put(truck.getName(), truck);
		minerComparator = new MinerComparator(map);
	}

	@Test
	void testCompareOnlyResources(){
		List<CraftedItem> emptyList = new ArrayList<>();
		Miner miner = new Miner(1, null, 1, 5, 10);
		miner.setCraftedItems(emptyList);
		Miner minerEqual = new Miner(2, null, 1, 4, 12);
		minerEqual.setCraftedItems(emptyList);
		Miner minerLess = new Miner(2, null, 1, 4, 11);
		minerLess.setCraftedItems(emptyList);
		Miner minerGreater = new Miner(2, null, 2, 0, 11);
		minerGreater.setCraftedItems(emptyList);
		assertAll(
				()->assertEquals(0, minerComparator.compare(miner, minerEqual)), // equal
				()->assertThat(minerComparator.compare(miner, minerLess), greaterThan(0)),
				()->assertThat(minerComparator.compare(miner, minerGreater), lessThan(0))
		);
	}

	@Test
	void testCompareOnlyItemsEqual(){
		Miner miner1 = new Miner(1, null, 0, 0, 0);
		List<CraftedItem> miner1list = new ArrayList<>();
		miner1list.add(drill);
		miner1.setCraftedItems(miner1list);

		Miner miner2 = new Miner(2, null, 0, 0, 0);
		List<CraftedItem> miner2list = new ArrayList<>();
		miner2list.add(drill);
		miner2.setCraftedItems(miner2list);

		assertEquals(0, minerComparator.compare(miner1, miner2));
	}

	@Test
	void testCompareOnlyItemsFirstGreaterMoreExpensive(){
		Miner miner1 = new Miner(1, null, 0, 0, 0);
		List<CraftedItem> miner1list = new ArrayList<>();
		miner1list.add(truck);
		miner1.setCraftedItems(miner1list);

		Miner miner2 = new Miner(2, null, 0, 0, 0);
		List<CraftedItem> miner2list = new ArrayList<>();
		miner2list.add(drill);
		miner2.setCraftedItems(miner2list);

		assertThat(minerComparator.compare(miner1, miner2), greaterThan(0));
	}

	@Test
	void testCompareOnlyItemsFirstLessMoreExpensive(){
		Miner miner1 = new Miner(1, null, 0, 0, 0);
		List<CraftedItem> miner1list = new ArrayList<>();
		miner1list.add(drill);
		miner1.setCraftedItems(miner1list);

		Miner miner2 = new Miner(2, null, 0, 0, 0);
		List<CraftedItem> miner2list = new ArrayList<>();
		miner2list.add(truck);
		miner2.setCraftedItems(miner2list);

		assertThat(minerComparator.compare(miner1, miner2), lessThan(0));
	}

	@Test
	void testCompareOnlyItemsFirstGreaterMoreItems(){
		Miner miner1 = new Miner(1, null, 0, 0, 0);
		List<CraftedItem> miner1list = new ArrayList<>();
		miner1list.add(drill);
		miner1list.add(drill);
		miner1list.add(drill);
		miner1list.add(drill);
		miner1list.add(drill);
		miner1.setCraftedItems(miner1list);

		Miner miner2 = new Miner(2, null, 0, 0, 0);
		List<CraftedItem> miner2list = new ArrayList<>();
		miner2list.add(truck);
		miner2.setCraftedItems(miner2list);

		assertThat(minerComparator.compare(miner1, miner2), greaterThan(0));
	}

	@Test
	void testCompareOnlyItemsFirstLessMoreItems(){
		Miner miner1 = new Miner(1, null, 0, 0, 0);
		List<CraftedItem> miner1list = new ArrayList<>();
		miner1list.add(truck);
		miner1.setCraftedItems(miner1list);

		Miner miner2 = new Miner(2, null, 0, 0, 0);
		List<CraftedItem> miner2list = new ArrayList<>();
		miner2list.add(drill);
		miner2list.add(drill);
		miner2list.add(drill);
		miner2list.add(drill);
		miner2list.add(drill);
		miner2.setCraftedItems(miner2list);

		assertThat(minerComparator.compare(miner1, miner2), lessThan(0));
	}

	@Test
	void testCompareItemsAndResourcesEqual(){
		Miner miner1 = new Miner(1, null, 0, 0, 0);
		List<CraftedItem> miner1list = new ArrayList<>();
		miner1list.add(truck);
		miner1.setCraftedItems(miner1list);

		Miner miner2 = new Miner(2, null, truck.getOilNeeded() - 1, truck.getIronNeeded() + 4, truck.getCopperNeeded() + 2);
		List<CraftedItem> miner2list = new ArrayList<>();
		miner2.setCraftedItems(miner2list);

		assertEquals(0, minerComparator.compare(miner1, miner2));
	}

	@Test
	void testCompareItemsAndResourcesFirstGreater(){
		Miner miner1 = new Miner(1, null, 0, 0, 0);
		List<CraftedItem> miner1list = new ArrayList<>();
		miner1list.add(truck);
		miner1.setCraftedItems(miner1list);

		Miner miner2 = new Miner(2, null, truck.getOilNeeded() - 1, truck.getIronNeeded(), truck.getCopperNeeded() + 2);
		List<CraftedItem> miner2list = new ArrayList<>();
		miner2.setCraftedItems(miner2list);

		assertThat(minerComparator.compare(miner1, miner2), greaterThan(0));
	}

	@Test
	void testCompareItemsAndResourcesFirstLess(){
		Miner miner1 = new Miner(1, null, 0, 0, 0);
		List<CraftedItem> miner1list = new ArrayList<>();
		miner1list.add(truck);
		miner1.setCraftedItems(miner1list);

		Miner miner2 = new Miner(2, null, truck.getOilNeeded() - 1, truck.getIronNeeded() + 5, truck.getCopperNeeded() + 2);
		List<CraftedItem> miner2list = new ArrayList<>();
		miner2.setCraftedItems(miner2list);

		assertThat(minerComparator.compare(miner1, miner2), lessThan(0));
	}
}
