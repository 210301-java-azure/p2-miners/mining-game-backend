package dev.miners.services;

import dev.miners.data.MinerRepository;
import dev.miners.exceptions.InvalidRequestException;
import dev.miners.models.CraftedItem;
import dev.miners.models.Mine;
import dev.miners.models.MineType;
import dev.miners.models.Miner;
import dev.miners.util.EconomyConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class MinerServiceTest {

	@InjectMocks
	private MinerService minerService;
	@MockBean
	private MinerRepository minerRepository;
	private final Mine mine = new Mine(123, MineType.OIL, 3.5);
	private final Miner oldMiner = new Miner(123, mine, 10, 50, 100);

	/*
	====================================================================================================================
												Update Miner
	====================================================================================================================
	 */

	@Test
	void updateMinerWithEmptyMinerReturnsOldMiner(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		Miner actual = minerService.updateMinerById(oldMiner.getId(), new Miner());
		assertEquals(oldMiner, actual);
	}

	@Test
	void updateMinerOnlyMine(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		Miner input = new Miner();
		input.setMine(new Mine(3, MineType.IRON, 156.2));
		Miner expected = new Miner(oldMiner.getId(), input.getMine(), oldMiner.getOil(), oldMiner.getIron(), oldMiner.getCopper());
		Miner actual = minerService.updateMinerById(oldMiner.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void updateMinerOnlyOil(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		Miner input = new Miner();
		input.setOil(123456789);
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), input.getOil(), oldMiner.getIron(), oldMiner.getCopper());
		Miner actual = minerService.updateMinerById(oldMiner.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void updateMinerOnlyIron(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		Miner input = new Miner();
		input.setIron(123456789);
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil(), input.getIron(), oldMiner.getCopper());
		Miner actual = minerService.updateMinerById(oldMiner.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void updateMinerOnlyCopper(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		Miner input = new Miner();
		input.setCopper(123456789);
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil(), oldMiner.getIron(), input.getCopper());
		Miner actual = minerService.updateMinerById(oldMiner.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void updateMinerOnlyCraftedItems(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		Miner input = new Miner();
		ArrayList<CraftedItem> list = new ArrayList<>();
		list.add(new CraftedItem());
		input.setCraftedItems(list);
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil(), oldMiner.getIron(), oldMiner.getCopper());
		expected.setCraftedItems(list);
		Miner actual = minerService.updateMinerById(oldMiner.getId(), input);
		assertEquals(expected, actual);
	}


	/*
	====================================================================================================================
												Trade with Computer
	====================================================================================================================
	 */
	@Test
	void testTradeWithComputerInvalidMineTypeInput() {
		assertAll(
				() -> assertThrows(InvalidRequestException.class, () -> minerService.tradeResourcesWithComputer(4, null, MineType.OIL, 10)),
				() -> assertThrows(InvalidRequestException.class, () -> minerService.tradeResourcesWithComputer(4, MineType.OIL, null, 10)),
				() -> assertThrows(InvalidRequestException.class, () -> minerService.tradeResourcesWithComputer(4, MineType.OIL, MineType.OIL, 10)),
				() -> assertThrows(InvalidRequestException.class, () -> minerService.tradeResourcesWithComputer(4, MineType.IRON, MineType.IRON, 10)),
				() -> assertThrows(InvalidRequestException.class, () -> minerService.tradeResourcesWithComputer(4, MineType.COPPER, MineType.COPPER, 10))
		);
	}

	@Test
	void testTradeWithComputerInvalidAmount() {
		assertAll(
				() -> assertThrows(InvalidRequestException.class, () -> minerService.tradeResourcesWithComputer(4, MineType.COPPER, MineType.OIL, 0)),
				() -> assertThrows(InvalidRequestException.class, () -> minerService.tradeResourcesWithComputer(4, MineType.COPPER, MineType.OIL, -34))
		);
	}

	@Test
	void testTradeWithComputerNotEnoughOil() {
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		assertThrows(InvalidRequestException.class, () -> minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.OIL, MineType.IRON, oldMiner.getOil() + 0.1));
	}

	@Test
	void testTradeWithComputerOilForIron() {
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		double tradeAmount = 5.5;
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil() - tradeAmount, oldMiner.getIron() + (tradeAmount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_IRON_PER_OIL), oldMiner.getCopper());
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.OIL, MineType.IRON, tradeAmount);
		assertAll(
				() -> assertEquals(expected.getOil(), actual.getOil()),
				() -> assertEquals(expected.getIron(), actual.getIron()),
				() -> assertEquals(expected.getCopper(), actual.getCopper())
		);
	}

	@Test
	void tradeWithComputerOilForCopper() {
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		double tradeAmount = 5.5;
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil() - tradeAmount, oldMiner.getIron(), oldMiner.getCopper() + (tradeAmount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_COPPER_PER_OIL));
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.OIL, MineType.COPPER, tradeAmount);
		assertAll(
				() -> assertEquals(expected.getOil(), actual.getOil()),
				() -> assertEquals(expected.getIron(), actual.getIron()),
				() -> assertEquals(expected.getCopper(), actual.getCopper())
		);
	}

	@Test
	void tradeWithComputerNotEnoughIron() {
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		assertThrows(InvalidRequestException.class, ()-> minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.IRON, MineType.OIL, oldMiner.getIron() + 0.1));
	}

	@Test
	void tradeWithComputerIronForOil(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		double tradeAmount = 45.8;
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil() + (tradeAmount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_OIL_PER_IRON), oldMiner.getIron() - tradeAmount, oldMiner.getCopper());
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.IRON, MineType.OIL, tradeAmount);
		assertAll(
				() -> assertEquals(expected.getOil(), actual.getOil()),
				() -> assertEquals(expected.getIron(), actual.getIron()),
				() -> assertEquals(expected.getCopper(), actual.getCopper())
		);
	}

	@Test
	void tradeWithComputerIronForCopper(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		double tradeAmount = 45.8;
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil(), oldMiner.getIron() - tradeAmount, oldMiner.getCopper() + (tradeAmount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_COPPER_PER_IRON));
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.IRON, MineType.COPPER, tradeAmount);
		assertAll(
				() -> assertEquals(expected.getOil(), actual.getOil()),
				() -> assertEquals(expected.getIron(), actual.getIron()),
				() -> assertEquals(expected.getCopper(), actual.getCopper())
		);
	}

	@Test
	void tradeWithComputerNotEnoughCopper() {
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		assertThrows(InvalidRequestException.class, ()->minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.COPPER, MineType.OIL, oldMiner.getCopper() + 0.1));
	}

	@Test
	void tradeWithComputerCopperForOil(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		double tradeAmount = 95.1;
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil() + (tradeAmount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_OIL_PER_COPPER), oldMiner.getIron(), oldMiner.getCopper() - tradeAmount);
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.COPPER, MineType.OIL, tradeAmount);
		assertAll(
				() -> assertEquals(expected.getOil(), actual.getOil()),
				() -> assertEquals(expected.getIron(), actual.getIron()),
				() -> assertEquals(expected.getCopper(), actual.getCopper())
		);
	}

	@Test
	void tradeWithComputerCopperForIron(){
		Miner oldMiner = new Miner(this.oldMiner.getId(), this.oldMiner.getMine(), this.oldMiner.getOil(), this.oldMiner.getIron(), this.oldMiner.getCopper());
		when(minerRepository.findById(oldMiner.getId())).thenReturn(Optional.of(oldMiner));
		double tradeAmount = 95.1;
		Miner expected = new Miner(oldMiner.getId(), oldMiner.getMine(), oldMiner.getOil(), oldMiner.getIron() + (tradeAmount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_IRON_PER_COPPER), oldMiner.getCopper() - tradeAmount);
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.tradeResourcesWithComputer(oldMiner.getId(), MineType.COPPER, MineType.IRON, tradeAmount);
		assertAll(
				() -> assertEquals(expected.getOil(), actual.getOil()),
				() -> assertEquals(expected.getIron(), actual.getIron()),
				() -> assertEquals(expected.getCopper(), actual.getCopper())
		);
	}

	/*
	====================================================================================================================
												Update Resources
	====================================================================================================================
	 */

	@Test
	void updateResourcesOil(){
		Mine mine = new Mine(123, MineType.OIL, 3.4);
		Miner miner = new Miner(123, mine, 1, 5, 10);
		when(minerRepository.findById(miner.getId())).thenReturn(Optional.of(miner));
		double amount = 123.456;
		Miner expected = new Miner(miner.getId(), mine, miner.getOil() + amount, miner.getIron(), miner.getCopper());
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.updateResourcesById(miner.getId(), amount);
		assertEquals(expected, actual);
	}

	@Test
	void updateResourcesIron(){
		Mine mine = new Mine(123, MineType.IRON, 3.4);
		Miner miner = new Miner(123, mine, 1, 5, 10);
		when(minerRepository.findById(miner.getId())).thenReturn(Optional.of(miner));
		double amount = 123.456;
		Miner expected = new Miner(miner.getId(), mine, miner.getOil(), miner.getIron() + amount, miner.getCopper());
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.updateResourcesById(miner.getId(), amount);
		assertEquals(expected, actual);
	}

	@Test
	void updateResourcesCopper(){
		Mine mine = new Mine(123, MineType.COPPER, 3.4);
		Miner miner = new Miner(123, mine, 1, 5, 10);
		when(minerRepository.findById(miner.getId())).thenReturn(Optional.of(miner));
		double amount = 123.456;
		Miner expected = new Miner(miner.getId(), mine, miner.getOil(), miner.getIron(), miner.getCopper() + amount);
		when(minerRepository.save(expected)).thenReturn(expected);
		Miner actual = minerService.updateResourcesById(miner.getId(), amount);
		assertEquals(expected, actual);
	}
}
