package dev.miners.services;

import dev.miners.data.MineRepository;
import dev.miners.models.Mine;
import dev.miners.models.MineType;
import dev.miners.util.EconomyConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class MineServiceTest {
	@InjectMocks
	private MineService mineService;
	@MockBean
	private MineRepository mineRepository;

	private static final Mine oldMine = new Mine(4, MineType.OIL, 2.6);


	@Test
	void testUpdateMineWithEmptyMineReturnsOldMine(){
		Mine mine = new Mine();
		when(mineRepository.findById(oldMine.getId())).thenReturn(Optional.of(oldMine));

		Mine actual = mineService.updateMineById(oldMine.getId(), mine);
		assertEquals(oldMine, actual);
	}

	@Test
	void testUpdateMineWithOnlyInterval(){
		Mine input = new Mine();
		input.setAmountPerInterval(542.24);
		when(mineRepository.findById(oldMine.getId())).thenReturn(Optional.of(oldMine));
		Mine expected = new Mine(oldMine.getId(), oldMine.getMineType(), input.getAmountPerInterval());
		Mine actual = mineService.updateMineById(oldMine.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void testUpdateWithOnlyMineType(){
		Mine input = new Mine();
		input.setMineType(MineType.COPPER);
		when(mineRepository.findById(oldMine.getId())).thenReturn(Optional.of(oldMine));
		Mine expected = new Mine(oldMine.getId(), input.getMineType(), EconomyConstants.COPPER_INITIAL_AMOUNT_PER_INTERVAL);
		Mine actual = mineService.updateMineById(oldMine.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void testUpdateWithBothOptions(){
		Mine input = new Mine(oldMine.getId(), MineType.COPPER, 154.24);
		when(mineRepository.findById(oldMine.getId())).thenReturn(Optional.of(oldMine));
		Mine actual = mineService.updateMineById(oldMine.getId(), input);
		assertEquals(input, actual);
	}

}
