package dev.miners.services;

import dev.miners.data.UserCredentialsRepository;
import dev.miners.exceptions.InvalidRequestException;
import dev.miners.managers.PasswordEncodingManager;
import dev.miners.models.Mine;
import dev.miners.models.MineType;
import dev.miners.models.Miner;
import dev.miners.models.UserCredentials;
import dev.miners.util.EconomyConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class UserCredentialsServiceTest {

	@InjectMocks
	private UserCredentialsService userCredentialsService;
	@MockBean
	private UserCredentialsRepository userCredentialsRepository;
	@MockBean
	private MinerService minerService;
	@MockBean
	private MineService mineService;
	@MockBean
	private ZipCodeService zipCodeService;
	@MockBean
	private PasswordEncodingManager passwordEncodingManager;

	private static final Miner oldMiner = new Miner(4, null, 25, 45, 100);
	private static final UserCredentials oldUser = new UserCredentials(4, "user", "user@web.com", "$2a$12$FFTPxrFVcDd2xuvG.EACPePahiV5wmSzjOIIerdfitE/3I194eHK2", oldMiner, "27253");

	/*
	====================================================================================================================
											add user
	====================================================================================================================
	 */
	@Test
	void testAddUserBadZipCode(){
		UserCredentials input = new UserCredentials();
		input.setZipCode("27222");
		input.setPassword("pass");
		when(zipCodeService.isValidZipCode("27222")).thenReturn(false);
		assertThrows(InvalidRequestException.class, ()->userCredentialsService.addUser(input, MineType.OIL));
	}

	@Test
	void testAddUserWithoutPassword(){
		UserCredentials input = new UserCredentials();
		assertThrows(InvalidRequestException.class, ()->userCredentialsService.addUser(input, MineType.OIL));
	}

	@Test
	void testAddUserOil(){
		UserCredentials input = new UserCredentials(0, "user", "user@web.com", "password", null, "27253");
		when(passwordEncodingManager.encryptString("password")).thenReturn("encoded");
		when(zipCodeService.isValidZipCode(input.getZipCode())).thenReturn(true);
		Mine resultMine = new Mine(4, MineType.OIL, EconomyConstants.OIL_INITIAL_AMOUNT_PER_INTERVAL);
		when(mineService.addMine(new Mine(0, MineType.OIL, EconomyConstants.OIL_INITIAL_AMOUNT_PER_INTERVAL)))
				.thenReturn(resultMine);
		Miner resultMiner = new Miner(4, resultMine, 0, 0, 0);
		when(minerService.addMiner(new Miner(0, resultMine, 0, 0, 0)))
				.thenReturn(resultMiner);
		UserCredentials expected = new UserCredentials(4, input.getUsername(), input.getEmail(), "encoded", resultMiner, input.getZipCode());
		when(userCredentialsRepository.save(new UserCredentials(input.getId(), input.getUsername(), input.getEmail(), "encoded", resultMiner, input.getZipCode()))).thenReturn(expected);
		UserCredentials actual = userCredentialsService.addUser(input, MineType.OIL);
		assertEquals(expected, actual);
	}

	@Test
	void testAddUserIron() {
		UserCredentials input = new UserCredentials(0, "user", "user@web.com", "password", null, "27253");
		when(passwordEncodingManager.encryptString("password")).thenReturn("encoded");
		when(zipCodeService.isValidZipCode(input.getZipCode())).thenReturn(true);
		Mine resultMine = new Mine(4, MineType.IRON, EconomyConstants.IRON_INITIAL_AMOUNT_PER_INTERVAL);
		when(mineService.addMine(new Mine(0, MineType.IRON, EconomyConstants.IRON_INITIAL_AMOUNT_PER_INTERVAL)))
				.thenReturn(resultMine);
		Miner resultMiner = new Miner(4, resultMine, 0, 0, 0);
		when(minerService.addMiner(new Miner(0, resultMine, 0, 0, 0)))
				.thenReturn(resultMiner);
		UserCredentials expected = new UserCredentials(4, input.getUsername(), input.getEmail(), "encoded", resultMiner, input.getZipCode());
		when(userCredentialsRepository.save(new UserCredentials(input.getId(), input.getUsername(), input.getEmail(), "encoded", resultMiner, input.getZipCode()))).thenReturn(expected);
		UserCredentials actual = userCredentialsService.addUser(input, MineType.IRON);
		assertEquals(expected, actual);
	}

	@Test
	void testAddUserCopper(){
		UserCredentials input = new UserCredentials(0, "user", "user@web.com", "password", null, "27253");
		when(passwordEncodingManager.encryptString("password")).thenReturn("encoded");
		when(zipCodeService.isValidZipCode(input.getZipCode())).thenReturn(true);
		Mine resultMine = new Mine(4, MineType.COPPER, EconomyConstants.COPPER_INITIAL_AMOUNT_PER_INTERVAL);
		when(mineService.addMine(new Mine(0, MineType.COPPER, EconomyConstants.COPPER_INITIAL_AMOUNT_PER_INTERVAL)))
				.thenReturn(resultMine);
		Miner resultMiner = new Miner(4, resultMine, 0, 0, 0);
		when(minerService.addMiner(new Miner(0, resultMine, 0, 0, 0)))
				.thenReturn(resultMiner);
		UserCredentials expected = new UserCredentials(4, input.getUsername(), input.getEmail(), "encoded", resultMiner, input.getZipCode());
		when(userCredentialsRepository.save(new UserCredentials(input.getId(), input.getUsername(), input.getEmail(), "encoded", resultMiner, input.getZipCode()))).thenReturn(expected);
		UserCredentials actual = userCredentialsService.addUser(input, MineType.COPPER);
		assertEquals(expected, actual);
	}

	/*
	====================================================================================================================
												Update
	====================================================================================================================
	 */

	@Test
	void testUpdateWithEmptyReturnsOld(){
		UserCredentials userCredentials = new UserCredentials();
		when(userCredentialsRepository.findById(oldUser.getId())).thenReturn(Optional.of(oldUser));
		when(userCredentialsRepository.save(oldUser)).thenReturn(oldUser);
		UserCredentials actual = userCredentialsService.updateCredentialsById(oldUser.getId(), userCredentials);
		assertEquals(oldUser, actual);
	}

	@Test
	void testUpdateOnlyUsername(){
		UserCredentials input = new UserCredentials();
		input.setUsername("new");
		when(userCredentialsRepository.findById(oldUser.getId())).thenReturn(Optional.of(oldUser));
		UserCredentials expected = new UserCredentials(oldUser.getId(), input.getUsername(), oldUser.getEmail(), oldUser.getPassword(), oldUser.getMiner(), oldUser.getZipCode());
		when(userCredentialsRepository.save(expected)).thenReturn(expected);
		UserCredentials actual = userCredentialsService.updateCredentialsById(oldUser.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void testUpdateOnlyPassword(){
		UserCredentials input = new UserCredentials();
		input.setPassword("new");
		when(userCredentialsRepository.findById(oldUser.getId())).thenReturn(Optional.of(oldUser));
		when(passwordEncodingManager.verifyPassword(input.getPassword(), oldUser.getPassword())).thenReturn(false);
		when(passwordEncodingManager.encryptString(input.getPassword())).thenReturn("encoded");
		UserCredentials expected = new UserCredentials(oldUser.getId(), oldUser.getUsername(), oldUser.getEmail(), "encoded", oldUser.getMiner(), oldUser.getZipCode());
		when(userCredentialsRepository.save(expected)).thenReturn(expected);
		UserCredentials actual = userCredentialsService.updateCredentialsById(oldUser.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void testUpdateOnlyEmail(){
		UserCredentials input = new UserCredentials();
		input.setEmail("new@email.com");
		when(userCredentialsRepository.findById(oldUser.getId())).thenReturn(Optional.of(oldUser));
		UserCredentials expected = new UserCredentials(oldUser.getId(), oldUser.getUsername(), input.getEmail(), oldUser.getPassword(), oldUser.getMiner(), oldUser.getZipCode());
		when(userCredentialsRepository.save(expected)).thenReturn(expected);
		UserCredentials actual = userCredentialsService.updateCredentialsById(oldUser.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void testUpdateOnlyZipCode(){
		UserCredentials input = new UserCredentials();
		input.setZipCode("98765");
		when(userCredentialsRepository.findById(oldUser.getId())).thenReturn(Optional.of(oldUser));
		when(zipCodeService.isValidZipCode(input.getZipCode())).thenReturn(true);
		UserCredentials expected = new UserCredentials(oldUser.getId(), oldUser.getUsername(), oldUser.getEmail(), oldUser.getPassword(), oldUser.getMiner(), input.getZipCode());
		when(userCredentialsRepository.save(expected)).thenReturn(expected);
		UserCredentials actual = userCredentialsService.updateCredentialsById(oldUser.getId(), input);
		assertEquals(expected, actual);
	}

	@Test
	void testUpdateOnlyZipCodeBadRequest(){
		UserCredentials input = new UserCredentials();
		input.setZipCode("12345");
		when(userCredentialsRepository.findById(oldUser.getId())).thenReturn(Optional.of(oldUser));
		when(zipCodeService.isValidZipCode(input.getZipCode())).thenReturn(false);
		assertThrows(InvalidRequestException.class, ()->userCredentialsService.updateCredentialsById(oldUser.getId(), input));
	}

	@Test
	void testUpdateOnlyMiner(){
		UserCredentials input = new UserCredentials();
		input.setMiner(new Miner(5, null, 5, 6, 7));
		when(userCredentialsRepository.findById(oldUser.getId())).thenReturn(Optional.of(oldUser));
		UserCredentials expected = new UserCredentials(oldUser.getId(), oldUser.getUsername(), oldUser.getEmail(), oldUser.getPassword(), input.getMiner(), oldUser.getZipCode());
		when(userCredentialsRepository.save(expected)).thenReturn(expected);
		UserCredentials actual = userCredentialsService.updateCredentialsById(oldUser.getId(), input);
		assertEquals(expected, actual);
	}
}
