package dev.miners.services;

import dev.miners.data.CraftedItemRepository;
import dev.miners.models.CraftedItem;
import dev.miners.models.Miner;
import dev.miners.util.EconomyConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class CraftedItemServiceTest {
    @InjectMocks
    private CraftedItemService craftedItemService;
    @MockBean
    private MinerService minerService;
    @MockBean
    private CraftedItemRepository craftedItemRepository;


    private static final Miner richMiner = new Miner(999, null, 100000, 100000, 100000);
    private static final CraftedItem drill = new CraftedItem("drill", 0.5, 0, 100, 250, 500, "desc", false);
    private static final CraftedItem truck = new CraftedItem("truck", 1, 0, 500, 1000, 2500, "desc", false);
    private static final CraftedItem tarps = new CraftedItem("tarps", 0, 0.05, 100, 250, 500, "desc", false);

    @Test
    void testCraftItem() {
        CraftedItem item = new CraftedItem("Test", 1, 0, 10, 15, 20, "Test Item", false);
        Miner modifiedMiner = new Miner();
        modifiedMiner.setId(999);
        List<CraftedItem> newItems = new ArrayList<>();
        newItems.add(item);
        modifiedMiner.setCraftedItems(newItems);
        List<CraftedItem> emptyItems = new ArrayList<>();
        richMiner.setCraftedItems(emptyItems);
        modifiedMiner.setIron(richMiner.getIron() - item.getIronNeeded());
        modifiedMiner.setOil(richMiner.getOil() - item.getOilNeeded());
        modifiedMiner.setCopper(richMiner.getCopper() - item.getCopperNeeded());
        when(craftedItemRepository.findById(item.getName())).thenReturn(Optional.of(item));
        when(minerService.getById(richMiner.getId())).thenReturn(richMiner);
        CraftedItem newItem = craftedItemService.craftItem(item.getName(), richMiner.getId());
        Mockito.verify(minerService).updateMinerById(modifiedMiner.getId(), modifiedMiner);
        assertEquals(item, newItem);
    }

    @Test
    void testCraftItemScalingRichMiner(){
        Miner miner = new Miner(richMiner.getId(), null, richMiner.getOil(), richMiner.getIron(), richMiner.getCopper());
        List<CraftedItem> list = new ArrayList<>(5);
        list.add(truck);
        list.add(truck);
        list.add(truck);
        list.add(truck);
        list.add(truck);
        miner.setCraftedItems(list);

        when(craftedItemRepository.findById(truck.getName())).thenReturn(Optional.of(truck));
        when(minerService.getById(miner.getId())).thenReturn(miner);

        CraftedItem expected = new CraftedItem(truck.getName(), truck.getAmountPerIntervalMultiplier(), truck.getWeatherEffectsModifier(), truck.getOilNeeded(), truck.getIronNeeded(), truck.getCopperNeeded(), truck.getDescription(), truck.isCosmetic());
        expected.setOilNeeded(truck.getOilNeeded()*Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 5));
        expected.setIronNeeded(truck.getIronNeeded()*Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 5));
        expected.setCopperNeeded(truck.getCopperNeeded()*Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 5));

        List<CraftedItem> endList = new ArrayList<>(6);
        endList.add(truck);
        endList.add(truck);
        endList.add(truck);
        endList.add(truck);
        endList.add(truck);
        endList.add(truck);
        Miner endMiner = new Miner(miner.getId(), null, richMiner.getOil()-expected.getOilNeeded(), richMiner.getIron()-expected.getIronNeeded(), richMiner.getCopper()-expected.getCopperNeeded());
        endMiner.setCraftedItems(endList);

        CraftedItem actual = craftedItemService.craftItem(truck.getName(), miner.getId());
        verify(minerService).updateMinerById(endMiner.getId(), endMiner);
        assertEquals(truck, actual);
    }

    @Test
    void testCraftItemScalingJustEnough(){
        Miner miner = new Miner(123, null, drill.getOilNeeded()*1.5, drill.getIronNeeded()*1.5, drill.getCopperNeeded()*1.5);
        List<CraftedItem> list = new ArrayList<>(1);
        list.add(drill);
        miner.setCraftedItems(list);

        when(craftedItemRepository.findById(drill.getName())).thenReturn(Optional.of(drill));
        when(minerService.getById(miner.getId())).thenReturn(miner);

        CraftedItem expected = new CraftedItem(drill.getName(), drill.getAmountPerIntervalMultiplier(), drill.getWeatherEffectsModifier(), drill.getOilNeeded(), drill.getIronNeeded(), drill.getCopperNeeded(), drill.getDescription(), drill.isCosmetic());
        expected.setOilNeeded(drill.getOilNeeded()*1.5);
        expected.setIronNeeded(drill.getIronNeeded()*1.5);
        expected.setCopperNeeded(drill.getCopperNeeded()*1.5);

        List<CraftedItem> endList = new ArrayList<>(2);
        endList.add(drill);
        endList.add(drill);
        Miner endMiner = new Miner(miner.getId(), null, 0, 0, 0);
        endMiner.setCraftedItems(endList);

        CraftedItem actual = craftedItemService.craftItem(drill.getName(), miner.getId());
        verify(minerService).updateMinerById(endMiner.getId(), endMiner);
        assertEquals(drill, actual);
    }

    @Test
    void testGetAllForMiner(){
        Miner miner = new Miner(123, null, 1000, 5000, 10000);
        List<CraftedItem> list = new ArrayList<>();
        list.add(drill);
        list.add(drill);
        list.add(drill);
        list.add(truck);
        miner.setCraftedItems(list);

        List<CraftedItem> allItems = new ArrayList<>();
        allItems.add(drill);
        allItems.add(truck);
        allItems.add(tarps);

        when(minerService.getById(miner.getId())).thenReturn(miner);
        when(craftedItemRepository.findAll()).thenReturn(allItems);

        CraftedItem drillCost = new CraftedItem(drill.getName(), drill.getAmountPerIntervalMultiplier(), drill.getWeatherEffectsModifier(), drill.getOilNeeded(), drill.getIronNeeded(), drill.getCopperNeeded(), drill.getDescription(), drill.isCosmetic());
        drillCost.setOilNeeded(drill.getOilNeeded() * Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 3));
        drillCost.setIronNeeded(drill.getIronNeeded() * Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 3));
        drillCost.setCopperNeeded(drill.getCopperNeeded() * Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 3));
        CraftedItem truckCost = new CraftedItem(truck.getName(), truck.getAmountPerIntervalMultiplier(), truck.getWeatherEffectsModifier(), truck.getOilNeeded(), truck.getIronNeeded(), truck.getCopperNeeded(), truck.getDescription(), truck.isCosmetic());
        truckCost.setOilNeeded(truck.getOilNeeded() * Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 1));
        truckCost.setIronNeeded(truck.getIronNeeded() * Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 1));
        truckCost.setCopperNeeded(truck.getCopperNeeded() * Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, 1));

        List<CraftedItem> actual = (List<CraftedItem>) craftedItemService.getAllForMiner(miner.getId());

        assertAll(
                ()->assertEquals(allItems.size(), actual.size()),
                ()->assertEquals(drillCost, actual.get(0)),
                ()->assertEquals(truckCost, actual.get(1)),
                ()->assertEquals(tarps, actual.get(2))
        );


    }


//    @Test
//    void testCraftItemNotEnoughResources(){
//
//    }
}
