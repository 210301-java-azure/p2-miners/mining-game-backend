package dev.miners.managers;

import dev.miners.exceptions.AuthenticationException;
import dev.miners.models.UserCredentials;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;

@Component
public class JwtTokenManager {

	private final Key key;
	private final Logger logger = LoggerFactory.getLogger(JwtTokenManager.class);

	public JwtTokenManager(){
		key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
	}


	public String issueToken(UserCredentials userCredentials){
		return Jwts.builder()
				.setId(String.valueOf(userCredentials.getId()))
				.setSubject(userCredentials.getUsername())
				.claim("role", userCredentials.isAdmin())
				.setIssuer("Miner Might")
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.signWith(key).compact();
	}

	public boolean authorize(int userId, String token){
		try {
		 	Claims c = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
		 	int uid = Integer.parseInt(c.getId());
		 	boolean admin = c.get("role", Boolean.class);
			return (admin || userId == uid);
		} catch (Exception e){
			logger.warn("JWT error when authorizing user");
			throw new AuthenticationException("JWT missing or invalid");
		}
	}

	public boolean authorizeAdmin(String token){
		try {
			Claims c = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
			return c.get("role", Boolean.class);
		} catch (Exception e){
			logger.warn("JWT error when authorizing admin");
			throw new AuthenticationException("JWT missing or invalid");
		}
	}

	public int parseUserIdFromToken(String token){
		try {
			return Integer.parseInt(Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getId());
		} catch (Exception e){
			logger.warn("JWT error parsing user id from token");
			throw new AuthenticationException("Unable to parse user id from JWT. Please sign in again");
		}
	}
}
