package dev.miners.managers;

import dev.miners.models.Mine;
import dev.miners.models.Miner;
import dev.miners.services.MineService;
import dev.miners.services.MinerService;
import dev.miners.services.WeatherService;
import dev.miners.util.EconomyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class WorldTimer {
    @Autowired
    MinerService minerService;
    @Autowired
    WeatherService weatherService;
    @Autowired
    MineService mineService;

    private static final int oneMinute = 60000;

    @Scheduled(fixedRate = oneMinute)
    public void emitMineSignal(){
        Iterable<Miner> miners = minerService.getAll();
        for (Miner miner:miners) {
            double efficiency = minerService.getMinerEfficiencyById(miner.getId());
            switch(miner.getMine().getMineType()){
                case OIL:
                    miner.setOil(efficiency + miner.getOil());
                    break;
                case IRON:
                    miner.setIron(efficiency + miner.getIron());
                    break;
                case COPPER:
                    miner.setCopper(efficiency + miner.getCopper());
                    break;
                default:
                    continue;

            }
            minerService.updateMinerById(miner.getId(), miner);
        }
    }

    @Scheduled(fixedRate = oneMinute * 60, initialDelay = 1000)
    public void emitWeatherCheckSignal() {
        //get all miners
        Iterable<Miner> miners = minerService.getAll();

        for (Miner miner : miners) {
            //for each miner. get the mine and set interval
            double amountPerInterval;
            switch(miner.getMine().getMineType()){
                case OIL:
                    amountPerInterval = EconomyConstants.OIL_INITIAL_AMOUNT_PER_INTERVAL;
                    break;
                case IRON:
                    amountPerInterval = EconomyConstants.IRON_INITIAL_AMOUNT_PER_INTERVAL;
                    break;
                case COPPER:
                    amountPerInterval = EconomyConstants.COPPER_INITIAL_AMOUNT_PER_INTERVAL;
                    break;
                default:
                    amountPerInterval = 0;
            }
            Mine mine = mineService.getById(miner.getMine().getId());
            double amount = amountPerInterval * weatherService.getTotalWeatherEfficiencyModifier(miner.getId());
            mine.setAmountPerInterval(amount);
            //update mine
            mineService.updateMineById(mine.getId(), mine);
        }
    }


}
