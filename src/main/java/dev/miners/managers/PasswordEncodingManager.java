package dev.miners.managers;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

@Component
public class PasswordEncodingManager {
	public String encryptString(String str) {
		return BCrypt.withDefaults().hashToString(12, str.toCharArray());
	}

	public boolean verifyPassword(String pw, String encryptedPassword) {
		if (pw == null) return false;
		return BCrypt.verifyer().verify(pw.toCharArray(), encryptedPassword).verified;
	}
}
