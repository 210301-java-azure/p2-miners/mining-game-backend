package dev.miners.exceptions;

public class AuthorizationException extends RuntimeException {

	public AuthorizationException() {
		super("User is Unauthorized to Access Requested Endpoint!");
	}

	public AuthorizationException(String message) {
		super(message);
	}

}
