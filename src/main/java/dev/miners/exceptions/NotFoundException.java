package dev.miners.exceptions;

public class NotFoundException extends RuntimeException{

	public NotFoundException() {
		super("No resource(s) found");
	}

	public NotFoundException(String message) {
		super(message);
	}
}
