package dev.miners.exceptions;

public class AuthenticationException extends RuntimeException{
	public AuthenticationException(){
		super("Invalid username and/or password");
	}

	public AuthenticationException(String message){
		super(message);
	}
}
