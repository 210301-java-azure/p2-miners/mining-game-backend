package dev.miners.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class WeatherDTO {
    @JsonProperty("id")
    private int sky;
    @JsonProperty("icon")
    private String icon;

    public WeatherDTO() {
    }

    public int getSky() {
        return sky;
    }

    public void setSky(int sky) {
        this.sky = sky;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "WeatherDTO{" +
                "sky='" + sky + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherDTO that = (WeatherDTO) o;
        return Objects.equals(sky, that.sky) && Objects.equals(icon, that.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sky, icon);
    }
}
