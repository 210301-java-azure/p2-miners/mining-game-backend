package dev.miners.dtos;

import java.util.Objects;

public class UserRankingDTO {
	private String username;
	private double score;

	public UserRankingDTO() {
	}

	public UserRankingDTO(String username, double score) {
		this.username = username;
		this.score = score;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UserRankingDTO that = (UserRankingDTO) o;
		return score == that.score && Objects.equals(username, that.username);
	}

	@Override
	public int hashCode() {
		return Objects.hash(username, score);
	}

	@Override
	public String toString() {
		return "UserRankingDTO{" +
				"username='" + username + '\'' +
				", score=" + score +
				'}';
	}
}
