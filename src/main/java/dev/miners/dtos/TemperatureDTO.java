package dev.miners.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class TemperatureDTO {
    @JsonProperty("temp")
    private double temperature;

    public TemperatureDTO() {
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "TemperatureDTO{" +
                "temperature=" + temperature +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemperatureDTO that = (TemperatureDTO) o;
        return temperature == that.temperature;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature);
    }
}
