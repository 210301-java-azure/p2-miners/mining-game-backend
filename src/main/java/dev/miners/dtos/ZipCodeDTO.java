package dev.miners.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZipCodeDTO {
	@JsonProperty("zip_code")
	private String zipCode;
	@JsonProperty("error_code")
	private String err;

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}

	@Override
	public String toString() {
		return "ZipCodeDTO{" +
				"zipCode='" + zipCode + '\'' +
				", err='" + err + '\'' +
				'}';
	}
}
