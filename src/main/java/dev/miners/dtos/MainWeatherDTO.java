package dev.miners.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Objects;

public class MainWeatherDTO {
    @JsonProperty("main")
    private TemperatureDTO temperatureData;
    @JsonProperty("weather")
    private List<WeatherDTO> weatherData;


    public MainWeatherDTO() {
    }

    public TemperatureDTO getTemperatureData() {
        return temperatureData;
    }

    public void setTemperatureData(TemperatureDTO temperatureData) {
        this.temperatureData = temperatureData;
    }

    public List<WeatherDTO> getWeatherData() {
        return weatherData;
    }

    public void setWeatherData(List<WeatherDTO> weatherData) {
        this.weatherData = weatherData;
    }

    @Override
    public String toString() {
        return "MainWeatherDTO{" +
                "temperatureData=" + temperatureData +
                ", weatherData=" + weatherData +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MainWeatherDTO that = (MainWeatherDTO) o;
        return Objects.equals(temperatureData, that.temperatureData) && Objects.equals(weatherData, that.weatherData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperatureData, weatherData);
    }
}
