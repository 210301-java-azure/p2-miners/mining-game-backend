package dev.miners.controllers;


import dev.miners.models.Mine;
import dev.miners.services.MineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping(path = "/mines")
public class MineController {
    @Autowired
    private MineService mineService;
    @PostMapping
    public Mine addMine(@RequestBody Mine mine){
        return mineService.addMine(mine);
    }

    @GetMapping
    public Iterable<Mine> getAll(){
        return mineService.getAll();
    }

    /**
     *
     * @param idNum
     * @return
     */
    @GetMapping(path="/{idNum}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mine getMineById(@PathVariable int idNum){
        return mineService.getById(idNum);
    }

    @PutMapping(path="/{idNum}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mine updateMineByIdAdminOnly(HttpServletRequest request, @PathVariable int idNum, @RequestBody Mine mine){
        return mineService.updateMineById(idNum, mine);
    }



}
