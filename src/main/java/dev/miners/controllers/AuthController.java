package dev.miners.controllers;

import dev.miners.managers.JwtTokenManager;
import dev.miners.models.UserCredentials;
import dev.miners.services.UserCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
@CrossOrigin
@RestController
public class AuthController {

	@Autowired
	private UserCredentialsService userCredentialsService;

	@Autowired
	private JwtTokenManager tokenManager;

	@PostMapping("/login")
	public UserCredentials login(@RequestBody UserCredentials userCredentials, HttpServletResponse response){
		userCredentials = userCredentialsService.authenticate(userCredentials);

		if (userCredentials != null) {

			String token = tokenManager.issueToken(userCredentials);

			response.addHeader("miner-token", token);
			response.addHeader("Access-Control-Expose-Headers", "miner-token");
			response.setStatus(200);

			return userCredentials;
		}
		response.setStatus(401);
		return null;
	}
}
