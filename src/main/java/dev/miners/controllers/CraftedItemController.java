package dev.miners.controllers;

import dev.miners.models.CraftedItem;
import dev.miners.services.CraftedItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
@RequestMapping(path = "/items")
public class CraftedItemController {

    @Autowired
    private CraftedItemService craftedItemService;

    @PostMapping
    public CraftedItem addItemAdminOnly(HttpServletRequest request, @RequestBody CraftedItem craftedItem){
        return craftedItemService.addItem(craftedItem);
    }

    @GetMapping
    public Iterable<CraftedItem> getAll( @RequestParam(value = "miner-id", required = false)Integer idNum){
        if (idNum == null){
            return craftedItemService.getAll();
        } else {
            return craftedItemService.getAllForMiner(idNum);
        }
    }


    @GetMapping(path="/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CraftedItem getItemByName(@PathVariable String name){
        return craftedItemService.getItemByName(name);
    }

    @GetMapping(path="/{itemName}/craft", produces = MediaType.APPLICATION_JSON_VALUE)
    public CraftedItem craftItem(@PathVariable String itemName, @RequestParam("miner-id") int idNum){
        return craftedItemService.craftItem(itemName, idNum);
    }}
