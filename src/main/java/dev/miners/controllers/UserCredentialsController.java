package dev.miners.controllers;

import dev.miners.models.MineType;
import dev.miners.models.UserCredentials;
import dev.miners.services.UserCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
@RequestMapping(path = "/users")
public class UserCredentialsController {
	@Autowired
	private UserCredentialsService userCredentialsService;

	@PostMapping
	public UserCredentials addUser(@RequestBody UserCredentials userCredentials, @RequestHeader("mine-type") MineType mineType){
		return userCredentialsService.addUser(userCredentials, mineType);
	}

	@GetMapping
	public Iterable<UserCredentials> getAllAdminOnly(HttpServletRequest request){
		return userCredentialsService.getAll();
	}

	@GetMapping(path="/{idNum}", produces = MediaType.APPLICATION_JSON_VALUE)
	public UserCredentials getUserByIdUserIdMatch(@PathVariable int idNum, HttpServletRequest request){
		return userCredentialsService.getById(idNum);
	}

	@PutMapping(path="/{idNum}", produces = MediaType.APPLICATION_JSON_VALUE)
	public UserCredentials updateUserByIdUserIdMatch(@PathVariable int idNum, HttpServletRequest request, @RequestBody UserCredentials userCredentials){
		return userCredentialsService.updateCredentialsById(idNum, userCredentials);
	}

	@DeleteMapping(path="/{idNum}")
	public void deleteUserByIdUserIdMatch(@PathVariable int idNum, HttpServletRequest request){
		userCredentialsService.deleteUser(idNum);
	}

	@GetMapping(path = "/{idNum}/set-admin")
	public UserCredentials updateUserAdminByIdAdminOnly(HttpServletRequest request, @PathVariable int idNum, @RequestParam("admin") boolean admin){
		return userCredentialsService.setAdmin(idNum, admin);
	}


}
