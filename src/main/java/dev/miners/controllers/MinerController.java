package dev.miners.controllers;

import dev.miners.dtos.UserRankingDTO;
import dev.miners.managers.JwtTokenManager;
import dev.miners.models.Findable;
import dev.miners.models.MineType;
import dev.miners.models.Miner;
import dev.miners.services.FindableService;
import dev.miners.services.MinerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping(path = "/miners")
public class MinerController {
	@Autowired
	private MinerService minerService;
	@Autowired
	private JwtTokenManager jwtTokenManager;
	@Autowired
    private FindableService findableService;

	@PostMapping
	public Miner addMiner(@RequestBody Miner miner) {
		return minerService.addMiner(miner);
	}

	@GetMapping
	public Iterable<Miner> getAll() {
		return minerService.getAll();
	}

	@GetMapping(path = "/{idNum}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Miner getMinerById(@PathVariable int idNum) {
		return minerService.getById(idNum);
	}

	@PutMapping(path = "/{idNum}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Miner updateMinerByIdAdminOnly(HttpServletRequest request, @PathVariable int idNum, @RequestBody Miner miner, HttpServletResponse response) {
		response.setStatus(200);
		return minerService.updateMinerById(idNum, miner);
	}

	// trade x of one for x of another

	@GetMapping("/{idNum}/trade")
	public Miner tradeWithComputerMinerIdMatch(
			@PathVariable("idNum") int minerId,
			HttpServletRequest request,
			@RequestParam("supply-type") MineType supplyType,
			@RequestParam("supply-amount") double amount,
			@RequestParam("return-type") MineType returnType
			) {
		return minerService.tradeResourcesWithComputer(minerId, supplyType, returnType, amount);
	}

    @GetMapping(path="/{idNum}/add")
    public List<Object> updateResourcesById(@PathVariable int idNum, @RequestParam("resource-amount") double amount, HttpServletResponse response, HttpServletRequest request){
        List<Object> objectsReturned = new ArrayList<>(2);
		objectsReturned.add(minerService.updateResourcesById(idNum, amount));
		Findable findable= findableService.checkForFindables(idNum);
        if(findable != null){
        	objectsReturned.add(findable);
		} else {
			objectsReturned.add(null);
		}
        return objectsReturned;
    }

	@GetMapping(path = "/{idNum}/efficiency")
	public double getMinerEfficiencyMinerIdMatch(@PathVariable int idNum, HttpServletRequest request) {
		return minerService.getMinerEfficiencyById(idNum);
	}


    @GetMapping(path="/{idNum}/weather")
    public Map<String, Object> getMinerWeatherByIdMinerIdMatch(@PathVariable int idNum, HttpServletRequest request, HttpServletResponse response) {
        return minerService.getFormattedWeatherList(idNum);
    }

    @GetMapping(path = "/leaderboard")
    public Iterable<UserRankingDTO> getLeaderboard(){
        return minerService.getLeaderboard();
    }
}

