package dev.miners.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;


@Entity
public class CraftedItem implements Serializable {
	@Id
	private String name; // umbrella, drill, truck
	private double amountPerIntervalMultiplier;
	private double weatherEffectsModifier;
	private boolean isCosmetic;
	private double oilNeeded;
	private double ironNeeded;
	private double copperNeeded;
	private String description;

	public CraftedItem() {
	}

	public CraftedItem(String name, double amountPerIntervalMultiplier, double weatherEffectsModifier, double oilNeeded, double ironNeeded, double copperNeeded, String description, boolean isCosmetic) {
		this.name = name;
		this.amountPerIntervalMultiplier = amountPerIntervalMultiplier;
		this.weatherEffectsModifier = weatherEffectsModifier;
		this.oilNeeded = oilNeeded;
		this.ironNeeded = ironNeeded;
		this.copperNeeded = copperNeeded;
		this.description = description;
		this.isCosmetic = isCosmetic;
	}

	public CraftedItem(String name, double amountPerIntervalMultiplier, double oilNeeded, double ironNeeded, double copperNeeded, String description) {
		this.name = name;
		this.amountPerIntervalMultiplier = amountPerIntervalMultiplier;
		this.oilNeeded = oilNeeded;
		this.ironNeeded = ironNeeded;
		this.copperNeeded = copperNeeded;
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CraftedItem that = (CraftedItem) o;
		return Double.compare(that.amountPerIntervalMultiplier, amountPerIntervalMultiplier) == 0 && oilNeeded == that.oilNeeded && ironNeeded == that.ironNeeded && copperNeeded == that.copperNeeded && Objects.equals(name, that.name) && Objects.equals(description, that.description);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, amountPerIntervalMultiplier, oilNeeded, ironNeeded, copperNeeded, description);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getAmountPerIntervalMultiplier() {
		return amountPerIntervalMultiplier;
	}

	public void setAmountPerIntervalMultiplier(double amountPerIntervalMultiplier) {
		this.amountPerIntervalMultiplier = amountPerIntervalMultiplier;
	}

	public double getOilNeeded() {
		return oilNeeded;
	}

	public void setOilNeeded(double oilNeeded) {
		this.oilNeeded = oilNeeded;
	}

	public double getIronNeeded() {
		return ironNeeded;
	}

	public void setIronNeeded(double ironNeeded) {
		this.ironNeeded = ironNeeded;
	}

	public double getCopperNeeded() {
		return copperNeeded;
	}

	public void setCopperNeeded(double copperNeeded) {
		this.copperNeeded = copperNeeded;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getWeatherEffectsModifier() {
		return weatherEffectsModifier;
	}

	public void setWeatherEffectsModifier(double weatherEffectsModifier) {
		this.weatherEffectsModifier = weatherEffectsModifier;
	}

	public boolean isCosmetic() {
		return isCosmetic;
	}

	public void setCosmetic(boolean cosmetic) {
		isCosmetic = cosmetic;
	}

	@Override
	public String toString() {
		return "CraftedItem{" +
				"name='" + name + '\'' +
				", amountPerIntervalMultiplier=" + amountPerIntervalMultiplier +
				", oilNeeded=" + oilNeeded +
				", ironNeeded=" + ironNeeded +
				", copperNeeded=" + copperNeeded +
				", description='" + description + '\'' +
				'}';
	}
}
