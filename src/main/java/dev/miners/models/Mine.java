package dev.miners.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
public class Mine implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private MineType mineType;
	private double amountPerInterval;

	public Mine(){
		this.amountPerInterval = -1;
	}


	public Mine(int id, MineType mineType, double amountPerInterval) {
		this.id = id;
		this.mineType = mineType;
		this.amountPerInterval = amountPerInterval;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Mine mine = (Mine) o;
		return id == mine.id && Double.compare(mine.amountPerInterval, amountPerInterval) == 0 && mineType == mine.mineType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, mineType, amountPerInterval);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public MineType getMineType() {
		return mineType;
	}

	public void setMineType(MineType mineType) {
		this.mineType = mineType;
	}

	public double getAmountPerInterval() {
		return amountPerInterval;
	}

	public void setAmountPerInterval(double amountPerInterval) {
		this.amountPerInterval = amountPerInterval;
	}

	@Override
	public String toString() {
		return "Mine{" +
				"id=" + id +
				", mineType=" + mineType +
				", amountPerInterval=" + amountPerInterval +
				'}';
	}
}
