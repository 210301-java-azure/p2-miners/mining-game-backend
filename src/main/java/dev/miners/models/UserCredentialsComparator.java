package dev.miners.models;

import java.util.Comparator;

public class UserCredentialsComparator implements Comparator<UserCredentials> {
	private final Comparator<Miner> minerComparator;

	public UserCredentialsComparator(Comparator<Miner> minerComparator){
		this.minerComparator = minerComparator;
	}

	public int compare(UserCredentials u1, UserCredentials u2){
		return minerComparator.compare(u1.getMiner(), u2.getMiner());
	}

}
