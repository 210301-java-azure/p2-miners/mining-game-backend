package dev.miners.models;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;


@Entity
public class Findable {
    @Id
    private String name;
    private double rarity;
    private String description;

    public void Findable(String name, double rarity, String description) {
        this.name = name;
        this.rarity = rarity;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRarity() {
        return rarity;
    }

    public void setRarity(double rarity) {
        this.rarity = rarity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Findable{" +
                "name='" + name + '\'' +
                ", rarity=" + rarity +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Findable findable = (Findable) o;
        return Double.compare(findable.rarity, rarity) == 0 && Objects.equals(name, findable.name) && Objects.equals(description, findable.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, rarity, description);
    }
}

