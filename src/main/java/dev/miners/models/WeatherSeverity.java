package dev.miners.models;

public enum WeatherSeverity {
    W_NONE, W_LOW, W_MEDIUM, W_HIGH, W_SEVERE
}
