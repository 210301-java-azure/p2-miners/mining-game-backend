package dev.miners.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
public class UserCredentials implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	/**
	 * Unique username
	 */
	@Column(unique = true)
	private String username;
	@Column(unique = true)
	private String email;
	private String password;
	@OneToOne
	private Miner miner;
	private String zipCode;
	@Column(columnDefinition = "bit default 0")
	private boolean admin;

	public UserCredentials() {
	}

	public UserCredentials(int id, String username, String email, String password, Miner miner, String zipCode) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.miner = miner;
		this.zipCode = zipCode;
	}

	public int getId() {
		return id;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Miner getMiner() {
		return miner;
	}

	public void setMiner(Miner miner) {
		this.miner = miner;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UserCredentials that = (UserCredentials) o;
		return id == that.id && admin == that.admin && Objects.equals(username, that.username) && Objects.equals(email, that.email) && Objects.equals(password, that.password) && Objects.equals(miner, that.miner) && Objects.equals(zipCode, that.zipCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, username, email, password, miner, zipCode, admin);
	}

	@Override
	public String toString() {
		return "UserCredentials{" +
				"id=" + id +
				", username='" + username + '\'' +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", miner=" + miner +
				", zipCode='" + zipCode + '\'' +
				", admin=" + admin +
				'}';
	}
}
