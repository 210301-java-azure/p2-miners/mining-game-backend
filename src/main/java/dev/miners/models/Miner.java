package dev.miners.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


@Entity
public class Miner implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@OneToOne
	private Mine mine;
	private double oil;
	private double iron;
	private double copper;
	@ManyToMany(fetch = FetchType.EAGER)
	@OrderColumn
	private List<CraftedItem> craftedItems;
	@ManyToMany(fetch = FetchType.EAGER)
	@OrderColumn
	private List<Findable> findables;

	public Miner() {
		this.mine = null;
		this.oil = -1;
		this.iron = -1;
		this.copper = -1;
		this.craftedItems = null;
	}

	public Miner(int id, Mine mine, double oil, double iron, double copper) {
		this.id = id;
		this.mine = mine;
		this.oil = oil;
		this.iron = iron;
		this.copper = copper;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Mine getMine() {
		return mine;
	}

	public void setMine(Mine mine) {
		this.mine = mine;
	}

	public double getOil() {
		return oil;
	}

	public void setOil(double oil) {
		this.oil = oil;
	}

	public double getIron() {
		return iron;
	}

	public void setIron(double iron) {
		this.iron = iron;
	}

	public double getCopper() {
		return copper;
	}

	public void setCopper(double copper) {
		this.copper = copper;
	}

	public List<CraftedItem> getCraftedItems() {
		return craftedItems;
	}

	public void setCraftedItems(List<CraftedItem> craftedItems) {
		this.craftedItems = craftedItems;
	}

	public List<Findable> getFindables() {
		return findables;
	}

	public void setFindables(List<Findable> findables) {
		this.findables = findables;
	}

	@Override
	public String toString() {
		return "Miner{" +
				"id=" + id +
				", mine=" + mine +
				", oil=" + oil +
				", iron=" + iron +
				", copper=" + copper +
				", craftedItems=" + craftedItems +
				", findables=" + findables +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Miner miner = (Miner) o;
		return id == miner.id && Double.compare(miner.oil, oil) == 0 && Double.compare(miner.iron, iron) == 0 && Double.compare(miner.copper, copper) == 0 && Objects.equals(mine, miner.mine) && Objects.equals(craftedItems, miner.craftedItems) && Objects.equals(findables, miner.findables);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, mine, oil, iron, copper, craftedItems, findables);
	}
}
