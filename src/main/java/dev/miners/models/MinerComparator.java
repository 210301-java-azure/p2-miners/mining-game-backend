package dev.miners.models;

import dev.miners.util.EconomyConstants;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MinerComparator implements Comparator<Miner> {

	private Map<String, CraftedItem> itemMap;

	/**
	 * Initialize the comparator with a map holding all the crafted items. Allows many comparisons
	 * without repeated DB calls to sort a list of Miners.
	 *
	 * @param itemMap Map that holds all items in the db, with the item name (pk) as the key
	 */
	public MinerComparator(Map<String, CraftedItem> itemMap) {
		this.itemMap = itemMap;
	}

	@Override
	public int compare(Miner o1, Miner o2) {
		double value1 = valueOfMiner(o1);
		double value2 = valueOfMiner(o2);
		return Double.compare(value1, value2);
	}


	public double valueOfMiner(Miner miner) {
		double totalValue = 0;
		// scale the totals according to their relative values
		totalValue += miner.getOil() 	/ EconomyConstants.OIL_INITIAL_AMOUNT_PER_INTERVAL;
		totalValue += miner.getIron() 	/ EconomyConstants.IRON_INITIAL_AMOUNT_PER_INTERVAL;
		totalValue += miner.getCopper() / EconomyConstants.COPPER_INITIAL_AMOUNT_PER_INTERVAL;
		// determine value of items
		Map<CraftedItem, Integer> minerMap = miner.getCraftedItems().stream().collect(Collectors.toMap(Function.identity(), v -> 1, Integer::sum));
		for (CraftedItem item : itemMap.values()){
			if (!minerMap.containsKey(item))
				continue;
			int numItem = minerMap.get(item);
			totalValue += totalCost(numItem, item.getOilNeeded())	/ EconomyConstants.OIL_INITIAL_AMOUNT_PER_INTERVAL;
			totalValue += totalCost(numItem, item.getIronNeeded()) 	/ EconomyConstants.IRON_INITIAL_AMOUNT_PER_INTERVAL;
			totalValue += totalCost(numItem, item.getCopperNeeded())/ EconomyConstants.COPPER_INITIAL_AMOUNT_PER_INTERVAL;
		}
		return totalValue;
	}

	/*
	Follows formula:
	f(x) = baseValue * (1 - (1 + growthRate)^numItems) / (1 - (1 + growthRate))
	f(x) is the total amount of resources spent on ALL numItems items
	 */
	private double totalCost(long numItems, double baseValue) {
		return baseValue * (1 - Math.pow(1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE, numItems)) /
				(1 - (1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE));

	}
}
