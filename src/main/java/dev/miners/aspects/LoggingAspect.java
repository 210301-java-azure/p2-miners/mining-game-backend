package dev.miners.aspects;

import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LoggingAspect {

	private final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Before("within(dev.miners.controllers.*)")
	public void logRequest(){
		HttpServletRequest request =
				((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		logger.info("{} {}", request.getMethod(), request.getRequestURI());
	}

	@Pointcut("execution(* dev.miners.managers.WorldTimer.emitMineSignal(..))")
	public void worldTimerPointcut(){}

	@AfterReturning("worldTimerPointcut()")
	public void logWorldTimer(){
		logger.info("World Timer has finished updating resources");
	}

	@AfterThrowing("worldTimerPointcut()")
	public void logWorldTimerException(){
		logger.warn("World Timer threw an exception while updating");
	}
}
