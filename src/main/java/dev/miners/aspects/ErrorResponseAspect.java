package dev.miners.aspects;

import dev.miners.exceptions.AuthenticationException;
import dev.miners.exceptions.AuthorizationException;
import dev.miners.exceptions.InvalidRequestException;
import dev.miners.exceptions.NotFoundException;
import dev.miners.dtos.ErrorResponse;
import dev.miners.util.ErrorResponseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Component
@RestControllerAdvice
public class ErrorResponseAspect {

	private ErrorResponseFactory errRespFactory;

	@Autowired
	public ErrorResponseAspect(ErrorResponseFactory errRespFactory) {
		this.errRespFactory = errRespFactory;
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ErrorResponse handleNotFoundException(NotFoundException e) {
		return errRespFactory.generateErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ErrorResponse handleUnauthorizedException(AuthorizationException e){
		return errRespFactory.generateErrorResponse(HttpStatus.UNAUTHORIZED, e.getMessage());
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ErrorResponse handleAuthenticationException(AuthenticationException e){
		return errRespFactory.generateErrorResponse(HttpStatus.UNAUTHORIZED, e.getMessage());
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleInvalidRequestException(InvalidRequestException e){
		return errRespFactory.generateErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
	}
}
