package dev.miners.aspects;

import dev.miners.exceptions.AuthorizationException;
import dev.miners.managers.JwtTokenManager;
import dev.miners.models.UserCredentials;
import dev.miners.services.UserCredentialsService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect
public class AuthorizationAspect {

	@Autowired
	private JwtTokenManager jwtTokenManager;
	@Autowired
	private UserCredentialsService userCredentialsService;
	private static final String TOKEN_HEADER = "miner-token";

	/**
	 * Used to authorize users to only view data relevant to themselves, or admins.
	 * Applies to controller methods ONLY in UserCredentialsController that include "UserIdMatch" in the method name
	 * These methods MUST have parameters set up as:
	 * 		- first parameter is the user id
	 * 		- second paramter is the HttpServletRequest
	 * @param joinPoint joinPoint of the method corresponded to the endpoint to secure
	 * @return Object the method returns
	 * @throws Throwable normal execution may throw exceptions
	 */
	@Around("execution(* dev.miners.controllers.UserCredentialsController.*UserIdMatch*(..))")
	public Object authorizeSameUser(ProceedingJoinPoint joinPoint) throws Throwable {
		int idNum = (int) joinPoint.getArgs()[0];
		HttpServletRequest request = (HttpServletRequest) joinPoint.getArgs()[1];
		String token = request.getHeader(TOKEN_HEADER);
		if (jwtTokenManager.authorize(idNum, token)) {
			return joinPoint.proceed();
		}
		throw new AuthorizationException();
	}

	/**
	 * Used to authorize users to only view or edit Miners that belong to them. (admins can still go through)
	 * Applies ONLY to methods in controllers package with "MinerIdMatch" in the method name
	 * These methods MUST have parameters set up as:
	 * 		- first parameter is miner id
	 * 		- second paramter is the HttpServletRequest
	 * @param joinPoint joinPoint of the method corresponded to the endpoint to secure
	 * @return Object the method returns
	 * @throws Throwable normal execution may throw exceptions
	 */
	@Around("execution(* dev.miners.controllers.*.*MinerIdMatch*(..))")
	public Object authorizeUserByMiner(ProceedingJoinPoint joinPoint) throws Throwable {
		int minerId = (int) joinPoint.getArgs()[0];
		HttpServletRequest request = (HttpServletRequest) joinPoint.getArgs()[1];
		String token = request.getHeader(TOKEN_HEADER);
		int userId = jwtTokenManager.parseUserIdFromToken(token);
		UserCredentials user = userCredentialsService.getById(userId);
		if (user.getMiner().getId() == minerId || jwtTokenManager.authorizeAdmin(token)) {
			return joinPoint.proceed();
		}
		throw new AuthorizationException(String.format("You can't alter miner %d since you are miner %d", minerId, user.getMiner().getId()));
	}

	/**
	 * Used to authorize only admins to access the endpoint.
	 * Applies to controller methods (Every class in controllers package) with "AdminOnly" in the method name
	 * These methods MUST have parameters set up as:
	 * 		- first parameter is HttpServletRequest
	 * @param joinPoint joinPoint of the method corresponded to the endpoint to secure
	 * @return Object the method returns
	 * @throws Throwable normal execution may throw exceptions
	 */
	@Around("execution(* dev.miners.controllers.*.*AdminOnly*(..))")
	public Object authorizeAdminOnly(ProceedingJoinPoint joinPoint) throws Throwable {
		HttpServletRequest request = (HttpServletRequest) joinPoint.getArgs()[0];
		String token = request.getHeader(TOKEN_HEADER);
		if (jwtTokenManager.authorizeAdmin(token)){
			return joinPoint.proceed();
		}
		throw new AuthorizationException();
	}


}
