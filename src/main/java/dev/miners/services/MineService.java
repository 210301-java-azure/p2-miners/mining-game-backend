package dev.miners.services;

import dev.miners.data.MineRepository;
import dev.miners.exceptions.NotFoundException;
import dev.miners.models.Mine;
import dev.miners.util.EconomyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MineService {
    @Autowired
    private MineRepository mineRepository;
    @Autowired
    private MinerService minerService;

    public Mine addMine(Mine mine) {
        return mineRepository.save(mine);
    }

    public Iterable<Mine> getAll() {
        return mineRepository.findAll();
    }

    public Mine getById(int idNum) {
        return mineRepository.findById(idNum).orElseThrow(NotFoundException::new);
    }

    public Mine updateMineById(int idNum, Mine mine) {
        Mine oldMine = getById(idNum);
        if (mine.getMineType() != null) {
            oldMine.setMineType(mine.getMineType());
            switch (mine.getMineType()) {
                case OIL:
                    oldMine.setAmountPerInterval(EconomyConstants.OIL_INITIAL_AMOUNT_PER_INTERVAL);
                    break;
                case IRON:
                    oldMine.setAmountPerInterval(EconomyConstants.IRON_INITIAL_AMOUNT_PER_INTERVAL);
                    break;
                case COPPER:
                    oldMine.setAmountPerInterval(EconomyConstants.COPPER_INITIAL_AMOUNT_PER_INTERVAL);
                    break;
            }
        }
        if (mine.getAmountPerInterval() != -1) {
            oldMine.setAmountPerInterval(mine.getAmountPerInterval());
        }
        mineRepository.save(oldMine);
        return oldMine;
    }

    public void deleteMine(Mine mine) {
        mineRepository.delete(mine);
    }

}

