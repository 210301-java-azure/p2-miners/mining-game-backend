package dev.miners.services;

import dev.miners.data.CraftedItemRepository;
import dev.miners.exceptions.InvalidRequestException;
import dev.miners.exceptions.NotFoundException;
import dev.miners.models.CraftedItem;
import dev.miners.models.Findable;
import dev.miners.models.Miner;
import dev.miners.util.EconomyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CraftedItemService {

	@Autowired
	private CraftedItemRepository craftedItemRepository;
	@Autowired
	private MinerService minerService;

	public CraftedItem addItem(CraftedItem craftedItem) {
		return craftedItemRepository.save(craftedItem);
	}

	public Iterable<CraftedItem> getAll() {
		return craftedItemRepository.findAll();
	}

	public CraftedItem getItemByName(String name) {
		return craftedItemRepository.findById(name).orElseThrow(NotFoundException::new);
	}

	public CraftedItem craftItem(String itemName, int minerId) {
		CraftedItem newItem = getItemByName(itemName);
		Miner miner = minerService.getById(minerId);

		if(itemName.equalsIgnoreCase("robot")){
			List<Findable> findables = miner.getFindables();
			int head = 0;
			int torso = 0;
			int legs = 0;
			int arms = 0;
			for(Findable findable: findables){
				switch(findable.getName()){
					case "Robot Head":
						head++;
						break;
					case "Robot Torso":
						torso++;
						break;
					case "Robot Arm":
						arms++;
						break;
					case "Robot Leg":
						legs++;
						break;
					default:
						continue;
				}
			}
			List<Findable> newFindables = new ArrayList<>();

			if(head >= 1 && torso >= 1 && legs >= 2 && arms >= 2){
				int headDecrement = 1;
				int torsoDecrement = 1;
				int legsDecrement = 2;
				int armsDecrement = 2;
				for(Findable findable: findables) {
					switch (findable.getName()) {
						case "Robot Head":
							if (headDecrement != 0) {
								headDecrement--;
							} else {
								newFindables.add(findable);
							}
							break;
						case "Robot Torso":
							if (torsoDecrement != 0) {
								torsoDecrement--;
							} else {
								newFindables.add(findable);
							}
							break;
						case "Robot Arm":
							if (armsDecrement != 0) {
								armsDecrement--;
							} else {
								newFindables.add(findable);
							}
							break;
						case "Robot Leg":
							if (legsDecrement != 0) {
								legsDecrement--;
							} else {
								newFindables.add(findable);
							}
							break;
						default:
							continue;
					}
				}
				miner.setFindables(newFindables);
				List<CraftedItem> craftedItems = miner.getCraftedItems();
				craftedItems.add(newItem);
				miner.setCraftedItems(craftedItems);
				minerService.updateMinerById(miner.getId(), miner);
				return newItem;
			} else {
				throw new InvalidRequestException("Not enough robot parts!");
			}

		}

		int numItemsOwned = (int) miner.getCraftedItems().stream().filter(item -> item.equals(newItem)).count();

		double ironNeeded = exponentialCost(numItemsOwned, newItem.getIronNeeded());
		double oilNeeded = exponentialCost(numItemsOwned, newItem.getOilNeeded());
		double copperNeeded = exponentialCost(numItemsOwned, newItem.getCopperNeeded());

		double ironHas = miner.getIron();
		double oilHas = miner.getOil();
		double copperHas = miner.getCopper();

		if (ironHas >= ironNeeded && oilHas >= oilNeeded && copperHas >= copperNeeded) {
			miner.setIron(ironHas - ironNeeded);
			miner.setOil(oilHas - oilNeeded);
			miner.setCopper(copperHas - copperNeeded);
			List<CraftedItem> craftedItems = miner.getCraftedItems();
			craftedItems.add(newItem);
			miner.setCraftedItems(craftedItems);
			minerService.updateMinerById(minerId, miner);
		} else {
			throw new InvalidRequestException("Not enough resources!");
		}
		return newItem;
	}

	public Iterable<CraftedItem> getAllForMiner(int minerId) {
		Miner miner = minerService.getById(minerId);
		Map<CraftedItem, Integer> minerMap = miner.getCraftedItems().stream().collect(Collectors.toMap(Function.identity(), v -> 1, Integer::sum));
		Iterable<CraftedItem> items = getAll();
		List<CraftedItem> customPrices = new ArrayList<>();
		items.forEach(item -> customPrices.add(itemPriceForMiner(item, minerMap)));
		return customPrices;
	}

	private CraftedItem itemPriceForMiner(CraftedItem item, Map<CraftedItem, Integer> minerMap) {
		if (!minerMap.containsKey(item))
			return item;
		int numItemOwned = minerMap.get(item);
		item.setOilNeeded(exponentialCost(numItemOwned, item.getOilNeeded()));
		item.setIronNeeded(exponentialCost(numItemOwned, item.getIronNeeded()));
		item.setCopperNeeded(exponentialCost(numItemOwned, item.getCopperNeeded()));
		return item;
	}

	/*
	Formula f(x) = baseAmount * (1 + growthRate)^numHas
	 */
	private double exponentialCost(int numHas, double baseAmount) {
		return baseAmount * Math.pow((1 + EconomyConstants.CRAFTED_ITEM_RESOURCE_GROWTH_RATE), numHas);
	}
}
