package dev.miners.services;


import dev.miners.data.FindableRepository;
import dev.miners.exceptions.NotFoundException;
import dev.miners.models.Findable;
import dev.miners.models.Mine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class FindableService {
    @Autowired
    private FindableRepository findableRepository;
    @Autowired
    private MinerService minerService;

    public Findable addFindableAdminOnly(Findable findable) {
        return findableRepository.save(findable);
    }

    public Iterable<Findable> getAll() {
        return findableRepository.findAll();
    }

    public Findable getItemByName(String name) {
        return findableRepository.findById(name).orElseThrow(NotFoundException::new);
    }

    public Findable checkForFindables(int idNum) {
        List<Findable> findables = (List<Findable>) getAll();
        Random rand = new Random();
        int totalChance = 0;
        List<Integer> chanceList = new ArrayList<>(findables.size());
        for(Findable findable: findables){
            totalChance += findable.getRarity() * 100;
            chanceList.add(totalChance);
        }
        int random = rand.nextInt(100);
            for(int i = 0; i < findables.size(); i++) {
                if (random < chanceList.get(i)) {
                    findables.get(i);
                    minerService.addFoundItem(idNum, findables.get(i));
                    return findables.get(i);
                }
            }
            return null;
        }
}