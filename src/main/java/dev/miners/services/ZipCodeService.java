package dev.miners.services;

import dev.miners.dtos.ZipCodeDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
@Service
public class ZipCodeService {
	// TODO: store this somewhere else
	private static final String ZIP_CODE_API_KEY = "uD4DmnNdjbhmltQuWnFOjqnYNjaqUdkSOMCfrWx0qBFs0MVDggXmeQdTqNmKqffZ";
	private static final String ZIP_CODE_TO_LOCATION_URL = "https://www.zipcodeapi.com/rest/" + ZIP_CODE_API_KEY + "/info.json/";
	private static final String ZIP_CODE_URL_END = "/degrees";

	/**
	 * Determines if the given zip code is a valid zip code
	 * @param zipCode string of zip code
	 * @return true iff response from api is 200
	 */
	public boolean isValidZipCode(String zipCode) {
		try {
			String requestUrl = ZIP_CODE_TO_LOCATION_URL + zipCode + ZIP_CODE_URL_END;
			RestTemplate restTemplate = new RestTemplate();
			ZipCodeDTO response = restTemplate.getForObject(requestUrl, ZipCodeDTO.class);
			return response != null && response.getErr() == null;
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)){
				return false;
			}
		}
		return false;
	}


}
