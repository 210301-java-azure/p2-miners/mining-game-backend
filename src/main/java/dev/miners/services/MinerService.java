package dev.miners.services;


import dev.miners.data.MinerRepository;
import dev.miners.dtos.MainWeatherDTO;
import dev.miners.dtos.UserRankingDTO;
import dev.miners.exceptions.InvalidRequestException;
import dev.miners.exceptions.NotFoundException;
import dev.miners.models.*;
import dev.miners.util.EconomyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class MinerService {
    @Autowired
    private MinerRepository minerRepository;
    @Autowired
    private CraftedItemService craftedItemService;
    @Autowired
    private WeatherService weatherService;
    @Autowired
    private UserCredentialsService userCredentialsService;
    @Autowired
    private MinerService minerService;

    public Miner addMiner(Miner miner) {
        return minerRepository.save(miner);
    }

    public Iterable<Miner> getAll() {
        return minerRepository.findAll();
    }

    public Miner getById(int idNum) {
        return minerRepository.findById(idNum).orElseThrow(NotFoundException::new);
    }

    public Miner updateMinerById(int idNum, Miner miner) {
        Miner oldMiner = getById(idNum);
        if(miner.getMine() != null){
            oldMiner.setMine(miner.getMine());
        }
        if(miner.getOil() != -1){
            oldMiner.setOil(miner.getOil());
        }
        if(miner.getIron() != -1){
            oldMiner.setIron(miner.getIron());
        }
        if(miner.getCopper() != -1){
            oldMiner.setCopper(miner.getCopper());
        }
        if(miner.getCraftedItems() != null){
            oldMiner.setCraftedItems(miner.getCraftedItems());
        }
        if(miner.getFindables() != null){
            oldMiner.setFindables(miner.getFindables());
        }
        minerRepository.save(oldMiner);
        return oldMiner;
    }

    public void deleteMiner(Miner miner) {
        minerRepository.delete(miner);
    }

    public Miner tradeResourcesWithComputer(int minerId, MineType sourceType, MineType resultType, double amount) {
        // types should not be null or NONE, and should not be the same type
        if (sourceType == null || resultType == null || sourceType == resultType) {
            throw new InvalidRequestException("Resource types specified to trade are invalid.");
        }
        if (amount <= 0)
            throw new InvalidRequestException("Amount to trade must be a positive number");

        Miner miner = minerRepository.findById(minerId).orElseThrow(NotFoundException::new);
        switch (sourceType) {
            case OIL:
                // determine if the miner has enough
                if (miner.getOil() < amount)
                    throw new InvalidRequestException(String.format("Miner %d with %f oil can not trade %f oil away", minerId, miner.getOil(), amount));
                // complete the trade
                if (resultType == MineType.IRON) {
                    // trade w/ iron
                    miner.setOil(miner.getOil() - amount);
                    miner.setIron(miner.getIron() + (amount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_IRON_PER_OIL));
                } else { // copper
                    miner.setOil(miner.getOil() - amount);
                    miner.setCopper(miner.getCopper() + (amount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_COPPER_PER_OIL));
                }
                break;
            case IRON:
                if (miner.getIron() < amount)
                    throw new InvalidRequestException(String.format("Miner %d with %f iron can not trade %f iron away", minerId, miner.getIron(), amount));
                if (resultType == MineType.OIL) {
                    miner.setIron(miner.getIron() - amount);
                    miner.setOil(miner.getOil() + (amount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_OIL_PER_IRON));
                } else { // copper
                    miner.setIron(miner.getIron() - amount);
                    miner.setCopper(miner.getCopper() + (amount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_COPPER_PER_IRON));
                }
                break;
            case COPPER:
                if (miner.getCopper() < amount)
                    throw new InvalidRequestException(String.format("Miner %d with %f copper can not trade %f copper away", minerId, miner.getCopper(), amount));
                if (resultType == MineType.OIL) {
                    miner.setCopper(miner.getCopper() - amount);
                    miner.setOil(miner.getOil() + (amount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_OIL_PER_COPPER));
                } else { // Iron
                    miner.setCopper(miner.getCopper() - amount);
                    miner.setIron(miner.getIron() + (amount * EconomyConstants.COMPUTER_TRADE_AMOUNT_OF_IRON_PER_COPPER));
                }
                break;
        }
        return minerRepository.save(miner);
    }

    public Miner updateResourcesById(int idNum, double amount) {
        Miner oldMiner = getById(idNum);
        MineType mineType = oldMiner.getMine().getMineType();
        switch (mineType) {
            case OIL:
                oldMiner.setOil(oldMiner.getOil() + amount);
                break;
            case IRON:
                oldMiner.setIron(oldMiner.getIron() + amount);
                break;
            case COPPER:
                oldMiner.setCopper(oldMiner.getCopper() + amount);
                break;
        }
        return minerRepository.save(oldMiner);
    }

    public Double getMinerEfficiencyById(int idNum) {
        Miner miner = getById(idNum);
        double amount = miner.getMine().getAmountPerInterval();
        List<CraftedItem> craftedItems = miner.getCraftedItems();
        double totalModifier = 1;
        for(CraftedItem craftedItem: craftedItems){
            if(craftedItem.getWeatherEffectsModifier() == 0){
                totalModifier += craftedItem.getAmountPerIntervalMultiplier();
            }
        }
        return totalModifier * amount;
    }

    public Double getMinerWeatherEffectsModifierById(int idNum) {
        Miner miner = getById(idNum);
        //get all miners crafted items
        List<CraftedItem> items = miner.getCraftedItems();
        double totalModifier = 0;
        for(CraftedItem item: items){
            //if item is a weather effect modifier then add to total modifier
            if(item.getWeatherEffectsModifier() != 0){
                totalModifier += item.getWeatherEffectsModifier();
            }
        } return totalModifier;
    }


    public Iterable<UserRankingDTO> getLeaderboard(){
        List<UserCredentials> list = new ArrayList<>();
        userCredentialsService.getAll().forEach(list::add);

        Map<String, CraftedItem> comparatorMap = new HashMap<>();
        craftedItemService.getAll().forEach(item -> comparatorMap.put(item.getName(), item));
        MinerComparator minerComparator = new MinerComparator(comparatorMap);
        Comparator<UserCredentials> userCredentialsComparator = new UserCredentialsComparator(minerComparator).reversed();

        list.sort(userCredentialsComparator);

        List<UserRankingDTO> rankingList = new ArrayList<>(list.size());
        for (UserCredentials userCredentials : list) {
            UserRankingDTO rankingDTO = new UserRankingDTO(userCredentials.getUsername(), minerComparator.valueOfMiner(userCredentials.getMiner()));
            rankingList.add(rankingDTO);
        }
        return rankingList;
    }

    public MainWeatherDTO getMinerWeatherById(int idNum){
        Miner miner = getById(idNum);
        String zipCode = userCredentialsService.getByMiner(miner).getZipCode();
        return weatherService.getWeather(zipCode);
    }

    public Map<String, Object> getFormattedWeatherList(int idNum) {
        Map<String, Object> weatherDetails = new HashMap<>();
        MainWeatherDTO weather = minerService.getMinerWeatherById(idNum);
        weatherDetails.put("Icon", weather.getWeatherData().get(0).getIcon());
        weatherDetails.put("Temp", weather.getTemperatureData().getTemperature());
        weatherDetails.put("Effect Reduction", minerService.getMinerWeatherEffectsModifierById(idNum));
        weatherDetails.put("Weather effect", weatherService.getTotalWeatherEfficiencyModifier(idNum));
        return weatherDetails;
    }

    public void addFoundItem(int idNum, Findable findable) {
        Miner miner = getById(idNum);
        List<Findable> findables = miner.getFindables();
        findables.add(findable);
        miner.setFindables(findables);
        updateMinerById(idNum, miner);
    }
}
