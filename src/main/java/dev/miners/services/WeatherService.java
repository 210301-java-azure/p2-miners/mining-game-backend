package dev.miners.services;

import dev.miners.dtos.MainWeatherDTO;
import dev.miners.models.Miner;
import dev.miners.models.WeatherSeverity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherService {
    private static final String WEATHER_API_KEY = "4c6f15aab7327c07457acdf8bb9ffe83";
    private static final String WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/weather?zip=";
    private static final String WEATHER_API_SECOND = "&appid=";
    private static final String WEATHER_API_METRIC = "&units=metric";
    @Autowired
    MinerService minerService;
    @Autowired
    UserCredentialsService userCredentialsService;

    public MainWeatherDTO getWeather(String zipCode) {
        try {
            String requestUrl = WEATHER_API_URL + zipCode + WEATHER_API_SECOND + WEATHER_API_KEY + WEATHER_API_METRIC ;
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.getForObject(requestUrl, MainWeatherDTO.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)){
                return null;
            }
        }
        return null;
    }

    public WeatherSeverity getWeatherSeverity(int sky){
        WeatherSeverity weatherSeverity = null;
        if(     sky >= 230 && sky <= 232 || //thunderstorm
                sky >= 300 && sky <= 321 || //increasing drizzle
                sky >= 520 && sky <= 521 || //increasing shower rain
                sky >= 701 && sky <= 762 || //particles in atmosphere
                sky == 500 || sky == 200 || //light rain || thunder light rain
                sky == 611 || sky == 620) { //sleet      || light shower/snow
            weatherSeverity = WeatherSeverity.W_LOW;

        } else
        if(sky == 201 || sky == 501 || sky == 522  //thunder rain    || moderate rain || heavy shower rain
                || sky == 600 || sky == 612  //light snow      || Light shower sleet
                || sky == 615 || sky == 621){//light rain/snow || Shower snow
            weatherSeverity = WeatherSeverity.W_MEDIUM;
        } else
        if(sky == 202 || sky >= 502 && sky <= 503   // tstorm heavy rain || very heavy rain
                || sky == 511 || sky == 531   // freezing rain     || ragged shower rain
                || sky == 601 || sky == 613   // snow              || Shower sleet
                || sky == 616 || sky == 622) {// rain and snow     || Heavy shower snow
            weatherSeverity = WeatherSeverity.W_HIGH;
        } else
        if(sky >= 771 && sky <= 781 || sky == 602 || sky == 504){//tornado/squals || heavy snow || extreme rain
            weatherSeverity = WeatherSeverity.W_SEVERE;
        } else {
            weatherSeverity = WeatherSeverity.W_NONE;
        }

        return weatherSeverity;
    }

    public Double getTotalWeatherEfficiencyModifier(int minerId){
        //get miner
        Miner miner = minerService.getById(minerId);
        MainWeatherDTO weather = getWeather(userCredentialsService.getByMiner(miner).getZipCode());
        double efficiencyModifier = 1;
        int sky = weather.getWeatherData().get(0).getSky();
        double temp = weather.getTemperatureData().getTemperature();
        WeatherSeverity weatherSeverity = getWeatherSeverity(sky);

        switch (weatherSeverity) {
            case W_NONE:
                efficiencyModifier = 1;
                break;
            case W_LOW:
                efficiencyModifier = 0.95;
                break;
            case W_MEDIUM:
                efficiencyModifier = 0.9;
                break;
            case W_HIGH:
                efficiencyModifier = 0.85;
                break;
            case W_SEVERE:
                efficiencyModifier = 0.75;
                break;
        }

        if (temp <= 0 || temp > 35) {
            efficiencyModifier -= 0.05;
        }

        efficiencyModifier += minerService.getMinerWeatherEffectsModifierById(minerId);
        if(efficiencyModifier > 1){
            efficiencyModifier = 1;
        }


        return efficiencyModifier;
    }

}
