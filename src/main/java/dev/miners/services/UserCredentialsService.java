package dev.miners.services;

import dev.miners.data.UserCredentialsRepository;
import dev.miners.exceptions.AuthenticationException;
import dev.miners.exceptions.InvalidRequestException;
import dev.miners.exceptions.NotFoundException;
import dev.miners.models.Mine;
import dev.miners.models.MineType;
import dev.miners.models.Miner;
import dev.miners.managers.PasswordEncodingManager;
import dev.miners.models.UserCredentials;
import dev.miners.util.EconomyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;


@Service
public class UserCredentialsService {

	@Autowired
	private UserCredentialsRepository userCredentialsRepository;
	@Autowired
	private MinerService minerService;
	@Autowired
	private MineService mineService;
	@Autowired
	private ZipCodeService zipCodeService;
	@Autowired
	private PasswordEncodingManager passwordEncodingManager;

	public UserCredentials addUser(UserCredentials userCredentials, MineType mineType){
		// encode password
		if (userCredentials.getPassword()==null)
			throw new InvalidRequestException("You must provide a password when registering a user");
		userCredentials.setPassword(passwordEncodingManager.encryptString(userCredentials.getPassword()));
		// validate zip code
		if (!zipCodeService.isValidZipCode(userCredentials.getZipCode())){
			throw new InvalidRequestException("Invalid zip code");
		}
		// Create new miner and mine automatically
		Mine newMine = new Mine();
		Miner newMiner = new Miner();
		newMiner.setOil(0);
		newMiner.setIron(0);
		newMiner.setCopper(0);
		newMine.setMineType(mineType);
		switch(mineType){
			case OIL:
				newMine.setAmountPerInterval(EconomyConstants.OIL_INITIAL_AMOUNT_PER_INTERVAL);
				break;
			case IRON:
				newMine.setAmountPerInterval(EconomyConstants.IRON_INITIAL_AMOUNT_PER_INTERVAL);
				break;
			case COPPER:
				newMine.setAmountPerInterval(EconomyConstants.COPPER_INITIAL_AMOUNT_PER_INTERVAL);
				break;
		}
		newMine = mineService.addMine(newMine);
		newMiner.setMine(newMine);
		newMiner = minerService.addMiner(newMiner);
		userCredentials.setMiner(newMiner);
		return userCredentialsRepository.save(userCredentials);
	}

	public Iterable<UserCredentials> getAll() {
		return userCredentialsRepository.findAll();
	}

	/**
	 * Get user information by user id
	 *
	 * @param idNum id of the user
	 * @return UserCredentials object of the requested user
	 */
	public UserCredentials getById(int idNum) {
		return userCredentialsRepository.findById(idNum).orElseThrow(NotFoundException::new);
	}

	public UserCredentials getByMiner(Miner miner) {
		return userCredentialsRepository.findByMiner(miner).orElseThrow(NotFoundException::new);
	}

	public UserCredentials updateCredentialsById(int idNum, UserCredentials userCredentials) {
		UserCredentials oldCredentials = getById(idNum);

		if (!oldCredentials.getUsername().equals(userCredentials.getUsername()) && userCredentials.getUsername() != null) {
			oldCredentials.setUsername(userCredentials.getUsername());
		}
		if (userCredentials.getPassword() != null && !passwordEncodingManager.verifyPassword(userCredentials.getPassword(), oldCredentials.getPassword())) {
			oldCredentials.setPassword(passwordEncodingManager.encryptString(userCredentials.getPassword()));
		}
		if (userCredentials.getEmail() != null && !oldCredentials.getEmail().equals(userCredentials.getEmail())) {
			oldCredentials.setEmail(userCredentials.getEmail());
		}
		if (userCredentials.getZipCode() != null && !oldCredentials.getZipCode().equals(userCredentials.getZipCode())) {
			if (!zipCodeService.isValidZipCode(userCredentials.getZipCode())){
				throw new InvalidRequestException("Invalid zip code");
			}
			oldCredentials.setZipCode(userCredentials.getZipCode());
		}
		if (userCredentials.getMiner() != null && !oldCredentials.getMiner().equals(userCredentials.getMiner())) {
			oldCredentials.setMiner(userCredentials.getMiner());
		}
		return userCredentialsRepository.save(oldCredentials);
	}
	public void deleteUser(int idNum) {
		Miner miner = getById(idNum).getMiner();
		Mine mine = miner.getMine();
		userCredentialsRepository.delete(getById(idNum));
		minerService.deleteMiner(miner);
		mineService.deleteMine(mine);
	}

	public UserCredentials authenticate(UserCredentials userCredentials){
		UserCredentials userInDb = userCredentialsRepository.findUserByUsername(userCredentials.getUsername()).orElseThrow(AuthenticationException::new);
		if (passwordEncodingManager.verifyPassword(userCredentials.getPassword(), userInDb.getPassword())){
			return userInDb;
		}
		throw new AuthenticationException();
	}

	public UserCredentials setAdmin(int userId, boolean admin) {
		UserCredentials userCredentials = getById(userId);
		userCredentials.setAdmin(admin);
		return userCredentialsRepository.save(userCredentials);
	}

}
