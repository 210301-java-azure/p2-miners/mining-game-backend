package dev.miners.util;

public class EconomyConstants {

	private EconomyConstants(){}

	public static final double OIL_INITIAL_AMOUNT_PER_INTERVAL = 1;
	public static final double IRON_INITIAL_AMOUNT_PER_INTERVAL = 5;
	public static final double COPPER_INITIAL_AMOUNT_PER_INTERVAL = 10;

	public static final double COMPUTER_TRADE_AMOUNT_OF_IRON_PER_OIL = 5;
	public static final double COMPUTER_TRADE_AMOUNT_OF_COPPER_PER_OIL = 10;
	public static final double COMPUTER_TRADE_AMOUNT_OF_OIL_PER_IRON = 0.2;
	public static final double COMPUTER_TRADE_AMOUNT_OF_COPPER_PER_IRON = 2;
	public static final double COMPUTER_TRADE_AMOUNT_OF_OIL_PER_COPPER = 0.1;
	public static final double COMPUTER_TRADE_AMOUNT_OF_IRON_PER_COPPER = 0.5;

	public static final double CRAFTED_ITEM_RESOURCE_GROWTH_RATE = 0.5;
}
