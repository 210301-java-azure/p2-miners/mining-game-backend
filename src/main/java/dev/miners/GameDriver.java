package dev.miners;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GameDriver {
	/*
	TODO:
	controllers for crud on models
	secure endpoints with JWT
	Add a miner / mine to a user account on

	constants class for app-wide constants

	H2 in memory DB for testing
	 */

	public static void main(String[] args) {

		SpringApplication.run(GameDriver.class, args);
	}
}
