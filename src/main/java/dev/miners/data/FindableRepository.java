package dev.miners.data;

import dev.miners.models.Findable;
import org.springframework.data.repository.CrudRepository;

public interface FindableRepository extends CrudRepository<Findable, String> {
}
