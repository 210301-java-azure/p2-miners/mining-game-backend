package dev.miners.data;

import dev.miners.models.Miner;
import org.springframework.data.repository.CrudRepository;

public interface MinerRepository extends CrudRepository<Miner, Integer> {
}
