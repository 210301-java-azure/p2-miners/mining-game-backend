package dev.miners.data;

import dev.miners.models.Mine;
import org.springframework.data.repository.CrudRepository;


public interface MineRepository extends CrudRepository<Mine, Integer> {
}
