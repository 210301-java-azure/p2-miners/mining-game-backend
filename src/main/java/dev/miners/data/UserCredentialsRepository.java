package dev.miners.data;

import dev.miners.models.Miner;
import dev.miners.models.UserCredentials;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UserCredentialsRepository extends CrudRepository<UserCredentials, Integer> {
	Optional<UserCredentials> findUserByUsername(String username);

    Optional<UserCredentials> findByMiner(Miner miner);
}
