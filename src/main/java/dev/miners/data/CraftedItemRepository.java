package dev.miners.data;

import dev.miners.models.CraftedItem;
import org.springframework.data.repository.CrudRepository;

public interface CraftedItemRepository extends CrudRepository<CraftedItem, String> {
}
